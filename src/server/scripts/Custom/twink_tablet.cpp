#include "twink_tablet.h"
#include "BattlegroundMgr.h"


std::unordered_map<std::string /*inventory type*/, std::pair<uint32 /*vendorID*/, uint32 /*vendorID for RBG items*/>> SlotVendorInfo = 
{
    {"twink_head", std::pair<uint32, uint32>(81000, 81015) },
    { "twink_chest", std::pair<uint32, uint32>(81001, 81016) }, // 5001 is vendor ID from npc_vendor, INVTYPE applies to type of item (head, chest, feet, weapons, etc.)
    // 5011 is vendor ID from npc_vendor for the invtype for RBG rating items
    //etc..
    { "twink_neck", std::pair<uint32, uint32>(81002, 81017) },
    { "twink_shoulders", std::pair<uint32, uint32>(81003, 81018) },
    { "twink_waist", std::pair<uint32, uint32>(81004, 81019) },
    { "twink_ranged_right", std::pair<uint32, uint32>(81005, 81020) },
    { "twink_legs", std::pair<uint32, uint32>(81006, 81021) },
    { "twink_feet", std::pair<uint32, uint32>(81007, 81022) },
    { "twink_wrists", std::pair<uint32, uint32>(81008, 81023) },
    { "twink_hands", std::pair<uint32, uint32>(81009, 81024) },
    { "twink_finger", std::pair<uint32, uint32>(81010, 81025) },
    { "twink_trinket", std::pair<uint32, uint32>(81011, 81026) },
    { "twink_weapon", std::pair<uint32, uint32>(81012, 81027) },
    { "twink_shield", std::pair<uint32, uint32>(81013, 81028) },
    { "twink_ranged", std::pair<uint32, uint32>(81005, 81020) },
    { "twink_2hweapon", std::pair<uint32, uint32>(81014, 81029) },
    { "twink_weapon_main_haind", std::pair<uint32, uint32>(81012, 81027) },
    { "twink_weapon_off_hand", std::pair<uint32, uint32>(81012, 81027) },
    { "twink_robe", std::pair<uint32, uint32>(81001, 81016) },
    { "twink_thrown", std::pair<uint32, uint32>(81005, 81020) },
    { "twink_holdable", std::pair<uint32, uint32>(81013, 81028) },
    { "twink_cloak", std::pair<uint32, uint32>(81030, 81031) }
};

typedef std::unordered_multimap<uint32 /*inventory type*/, uint32 /*enchant ID*/> EnchantMap;

EnchantMap EnchantIds;

std::unordered_map<uint32 /*item ID*/, uint32 /*Required RBG Rating*/> RatingItems;

const std::pair<uint32, std::string> petEntries[] = 
{
    std::make_pair(1130, "aSDFASDFASDF"),
    std::make_pair(36696, "aSDFASDFASDF"),
    std::make_pair(34369, "aSDFASDFASDF"),
    std::make_pair(2476, "aSDFASDFASDF"),
    std::make_pair(5823, "aSDFASDFASDF"),
    std::make_pair(44626, "aSDFASDFASDF")
};

std::multimap<uint32 /*item entry*/, int32 /*suffixId*/> ItemSuffixes;

/*end config*/


std::unordered_map<uint32 /*lowGUID*/, GossipStageInfo /*stage*/> PlayerStages;

bool PrepareEnchantList(Player* player, Item* itemPtr, const ItemTemplate* proto);
void Checkout(Player* player);

bool LearnAllRecipesInProfession(Player* player, SkillType skill)
{
    SkillLineEntry const* SkillInfo = sSkillLineStore.LookupEntry(skill);
    uint16 skill_level = 225;
    if (!SkillInfo)
    {
        sLog->outError(LOG_FILTER_PLAYER_SKILLS, TXT_BAD_SKILL);
        return false;
    }
    if (skill == SKILL_ENGINEERING)
        skill_level = 150;
    player->SetSkill(SkillInfo->id, player->GetSkillStep(SkillInfo->id), skill_level, skill_level);

    return true;
}

bool ForgotSkill(Player* player, SkillType skill)
{
    if (!player->HasSkill(skill))
        return false;
    SkillLineEntry const* SkillInfo = sSkillLineStore.LookupEntry(skill);
    if (!SkillInfo)
    {
        sLog->outError(LOG_FILTER_PLAYER_SKILLS, TXT_BAD_SKILL);
        return false;
    }
    player->SetSkill(SkillInfo->id, player->GetSkillStep(SkillInfo->id), 0, 0);

    return true;
}

bool PlayerAlreadyHasTwoProfessions(Player* player)
{
    uint32 skillCount = player->HasSkill(SKILL_MINING) + player->HasSkill(SKILL_SKINNING)
        + player->HasSkill(SKILL_HERBALISM) + player->HasSkill(SKILL_ENGINEERING);
    if (skillCount >= 2)
        return true;

    return false;
}

void CompleteLearnProfession(Player* player, SkillType skill)
{
    if (PlayerAlreadyHasTwoProfessions(player))
        player->GetSession()->SendNotification(TXT_ERR_MAX);
    else if (!LearnAllRecipesInProfession(player, skill))
        player->GetSession()->SendNotification(TXT_PROBLEM);
}

void Enchant(Player* player, uint32 enchantid, bool def = false)
{
    Item* item = player->GetItemByGuid(PlayerStages[player->GetGUIDLow()].enchantItemGuid);

    if (!item)
    {
        player->GetSession()->SendNotification("You must equip the item you would like to enchant in order to enchant it!");
        return;
    }

    if (!enchantid)
        return;

    player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, false);
    item->SetEnchantment(PERM_ENCHANTMENT_SLOT, enchantid, 0, 0);
    player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, true);
    if (def)
    {
        SpellItemEnchantmentEntry const* enchantEntry = sSpellItemEnchantmentStore.LookupEntry(enchantid);
        if (enchantEntry)
            player->GetSession()->SendAreaTriggerMessage("%s default enchanted with %s", item->GetTemplate()->Name1.c_str(), enchantEntry->description);
    }
    else
    {
        player->GetSession()->SendAreaTriggerMessage("%s Successfully enchanted!", item->GetTemplate()->Name1.c_str());
        player->PlayerTalkClass->SendCloseGossip();
    }
}

class SendSuffixGossip : public BasicEvent
{
public:
    SendSuffixGossip(Player* player, Item* item, uint32 entry) : m_player(player), m_item(item), m_entry(entry), BasicEvent() {}

    bool Execute(uint64 /*e_time*/, uint32 /*p_time*/)
    {
        std::pair<std::multimap<uint32 /*item entry*/, int32 /*suffixId*/>::iterator,
            std::multimap<uint32 /*item entry*/, int32 /*suffixId*/>::iterator> range = ItemSuffixes.equal_range(m_entry);

        if (range.first == range.second)
            return true;

        for (std::multimap<uint32 /*item entry*/, int32 /*suffixId*/>::iterator itr = range.first; itr != range.second; ++itr)
        {
            if (itr->second < 0)
            {
                ItemRandomSuffixEntry const* entry = sItemRandomSuffixStore.LookupEntry(-itr->second);
                if (entry)
                {
                    std::string suffixName = entry->nameSuffix;
                    m_player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "... " + suffixName, GOSSIP_SENDER_SUFFIX, entry->ID);
                }
            }
            else if (itr->second > 0)
            {
                auto entry = sItemRandomPropertiesStore.LookupEntry(itr->second);
                if (entry)
                {
                    std::string suffixName = entry->nameSuffix;
                    m_player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "... " + suffixName, GOSSIP_SENDER_RANDPROP, entry->ID);
                }
            }

        }

        if (!m_player->PlayerTalkClass->GetGossipMenu().GetMenuItems().empty())
        {
            if (m_player->PlayerTalkClass->GetGossipMenu().GetMenuItems().size() == 1)
                m_player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "No thanks..", GOSSIP_SENDER_MAIN, 0);

            PlayerStages[m_player->GetGUIDLow()].vendorStage = VENDOR_STAGE_PREFIX;
            m_player->PlayerTalkClass->SendGossipMenu("Please pick your suffix.", m_item->GetGUID()); // twice to fix client bug.

        }
        return true;
    }

    Player* m_player;
    Item* m_item;
    uint32 m_entry;
};

struct RbgItem_Sort
{
    bool operator()(const VendorItem* item1, const VendorItem* item2)
    {
        return std::less<uint32>()(RatingItems[item1->item], RatingItems[item2->item]);
    }
};

class twink_tablet_loader : public WorldScript
{
public:
    twink_tablet_loader() : WorldScript("twink_tablet_loader") {}
    void OnStartup() override
    {
        QueryResult suffixes = WorldDatabase.Query("SELECT * FROM tablet_suffix_ids");
        if (suffixes)
        {
            do{
                Field* fields = suffixes->Fetch();
                ItemSuffixes.insert(std::make_pair(fields[0].GetUInt32(), fields[1].GetInt32()));
            } while (suffixes->NextRow());
        }

        QueryResult ratingItems = WorldDatabase.Query("SELECT * FROM tablet_rating_items");
        if (ratingItems)
        {
            do{
                Field* fields = ratingItems->Fetch();
                RatingItems.insert(std::make_pair(fields[0].GetUInt32(), fields[1].GetUInt32()));
            } while (ratingItems->NextRow());
        }

        QueryResult enchants = WorldDatabase.Query("SELECT * FROM tablet_enchants");
        if (enchants)
        {
            do{
                Field* fields = enchants->Fetch();
                EnchantIds.insert(std::make_pair(fields[0].GetUInt32(), fields[1].GetUInt32()));
            } while (enchants->NextRow());
        }

    }
};

class twink_tablet : public ItemScript
{
public : 
	twink_tablet() : ItemScript("twink_tablet")
	{

	}

    void TamePet(Player* player, uint32 creatureEntry)
    {
        CreatureTemplate const* creatureTemplate = sObjectMgr->GetCreatureTemplate(creatureEntry);
        WorldSession* session = player->GetSession();

        if (!creatureTemplate->family)
        {
            session->SendNotification("This creature cannot be tamed.");
            return;
        }

        if (player->GetPetGUID())
        {
            session->SendNotification("You already have a pet!");
            return;
        }

        PetSlot newslot = player->getSlotForNewPet();
        if (newslot == PET_SLOT_FULL_LIST)
        {
            session->SendNotification("Too Many Pets!");
            return;
        }

        // Everything looks OK, create new pet
        Pet* pet = new Pet(player, HUNTER_PET);
        Creature* creatureTarget = player->SummonCreature(creatureEntry, player->GetPositionX(), player->GetPositionY(), player->GetPositionZ(), player->GetOrientation(), TEMPSUMMON_CORPSE_TIMED_DESPAWN, 500);
        if (!pet->CreateBaseAtCreature(creatureTarget))
        {
            delete pet;
            session->SendNotification("Internal error, sorry!");
            return;
        }

        creatureTarget->setDeathState(JUST_DIED);
        creatureTarget->RemoveCorpse();
        creatureTarget->SetHealth(0); // just for nice GM-mode view

        pet->SetUInt64Value(UNIT_FIELD_CREATEDBY, player->GetGUID());
        pet->SetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE, player->getFaction());

        if (!pet->InitStatsForLevel(creatureTarget->getLevel()))
        {
            sLog->outError(LOG_FILTER_GENERAL, "InitStatsForLevel() in EffectTameCreature failed! Pet deleted.");
            session->SendNotification("Internal error, sorry!");
            delete pet;
            return;
        }

        // prepare visual effect for levelup
        pet->SetUInt32Value(UNIT_FIELD_LEVEL, player->getLevel() - 1);

        pet->GetCharmInfo()->SetPetNumber(sObjectMgr->GeneratePetNumber(), true);
        // this enables pet details window (Shift+P)
        pet->InitPetCreateSpells();
        pet->SetFullHealth();

        pet->GetMap()->AddToMap(pet->ToCreature());

        // visual effect for levelup
        pet->SetUInt32Value(UNIT_FIELD_LEVEL, player->getLevel());

        player->SetMinion(pet, true, newslot);
        pet->SavePet(newslot);
        player->PetSpellInitialize();

        if (player->getClass() == CLASS_HUNTER)
            player->GetSession()->SendStablePet(0);
    }

    void SendCustomListInventory(WorldSession* session, uint32 entry, Item* item, const VendorItemList* alternativeItems = nullptr, bool RbgCheck = false)
    {
        sLog->outDebug(LOG_FILTER_NETWORKIO, "WORLD: Sent SMSG_LIST_INVENTORY");

        const VendorItemList* vendorItems = entry ? &sObjectMgr->GetNpcVendorItemList(entry)->m_items : alternativeItems;
        uint32 rawItemCount = vendorItems ? vendorItems->size() : 0;

        // remove fake death
        if (session->GetPlayer()->HasUnitState(UNIT_STATE_DIED))
            session->GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);


        if (!vendorItems)
            return;


        ByteBuffer itemsData(32 * rawItemCount);
        std::vector<bool> enablers;
        enablers.reserve(2 * rawItemCount);

        const float discountMod = 0.0f;
        uint8 count = 0;
        Player* _player = session->GetPlayer();
        uint32 rbgRating = _player->GetRBGPersonalRating();

        VendorItemList sortedItems;
        if (RbgCheck)
        {
             sortedItems = *vendorItems;
             std::sort(sortedItems.begin(), sortedItems.end(), RbgItem_Sort());
        }
        else
        {
            sortedItems = *vendorItems;
            VendorItem const* vendorItem = (*vendorItems)[0];
            if (!vendorItem)
                return;

            ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(vendorItem->item);
            if (!itemTemplate)
                return;

            if (itemTemplate->Class == ITEM_CLASS_ARMOR && itemTemplate->SubClass < ITEM_SUBCLASS_ARMOR_BUCKLER)
                std::sort(sortedItems.begin(), sortedItems.end(), [](const VendorItem* item1, const VendorItem* item2) {
                ItemTemplate const* item1Temp = sObjectMgr->GetItemTemplate(item1->item);
                if (!item1Temp)
                    return true;

                ItemTemplate const* item2Temp = sObjectMgr->GetItemTemplate(item2->item);
                if (!item2Temp)
                    return true;

                return std::greater<uint32>()(item1Temp->SubClass, item2Temp->SubClass);
            });

        }

        for (uint32 slot = 0; slot < rawItemCount; ++slot)
        {
            VendorItem const* vendorItem = sortedItems[slot];
            if (!vendorItem)
                continue;

            uint32 questItemCount = _player->GetItemCount(vendorItem->item, true);

            if (!_player->isGameMaster() && vendorItem->QuestItem && questItemCount >= vendorItem->QuestItem)
                continue; // no more than QuestItem count.

            if (vendorItem->Type == ITEM_VENDOR_TYPE_ITEM)
            {
                ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(vendorItem->item);
                if (!itemTemplate)
                    continue;

                if (entry == VENDOR_ID_METADATA && !_player->isGameMaster())
                {
                    bool allWrong = true;

                    auto checkItems = PlayerStages[_player->GetGUIDLow()].vendorStage == VENDOR_STAGE_PRESELECT_RBG ? SlotVendorInfo[sObjectMgr->GetScriptName(itemTemplate->ScriptId)].second : SlotVendorInfo[sObjectMgr->GetScriptName(itemTemplate->ScriptId)].first;
                    auto vendorListCheck = sObjectMgr->GetNpcVendorItemList(checkItems);
                    if (vendorListCheck)
                    {
                        auto listItems = vendorListCheck->m_items;
                        for (const auto& item : listItems)
                        {
                            auto checkTemplate = sObjectMgr->GetItemTemplate(item->item);
                            if (checkTemplate)
                            {
                                if (_player->CanUseItem(checkTemplate) == EQUIP_ERR_OK)
                                {
                                    allWrong = false;
                                    break;
                                }
                            }
                        }
                    }

                    if (allWrong)
                        continue;
                }

                //uint32 leftInStock = !vendorItem->maxcount ? 0xFFFFFFFF : vendor->GetVendorItemCurrentCount(vendorItem);

                uint32 leftInStock = 0xFFFFFFFF;

                if (RbgCheck)
                {
                    if (RatingItems[itemTemplate->ItemId] > rbgRating)
                        leftInStock = 0;
                }

                if (!_player->isGameMaster() && entry != VENDOR_ID_METADATA) // ignore conditions if GM on
                {
                    // Respect allowed class
                    if (!(itemTemplate->AllowableClass & _player->getClassMask()) && itemTemplate->Bonding == BIND_WHEN_PICKED_UP)
                        continue;

                    // Only display items in vendor lists for the team the player is on
                    if ((itemTemplate->Flags2 & ITEM_FLAGS_EXTRA_HORDE_ONLY && _player->GetTeam() == ALLIANCE) ||
                        (itemTemplate->Flags2 & ITEM_FLAGS_EXTRA_ALLIANCE_ONLY && _player->GetTeam() == HORDE))
                        continue;


                    if (_player->CanUseItem(itemTemplate) != EQUIP_ERR_OK)
                        continue;

                    if (itemTemplate->ItemId == 51964)
                    {
                        if (_player->getClass() != CLASS_ROGUE && _player->getClass() != CLASS_DRUID && _player->getClass() != CLASS_SHAMAN && _player->getClass() != CLASS_HUNTER) 
                            continue;
                    }

                    if (itemTemplate->ItemId == 51978)
                    {
                        if (_player->getClass() != CLASS_PALADIN && _player->getClass() != CLASS_WARRIOR)
                            continue;
                    }

                    if (itemTemplate->ItemId == 51968)
                    {
                        if (_player->getClass() != CLASS_WARLOCK && _player->getClass() != CLASS_MAGE && _player->getClass() != CLASS_PRIEST)
                            continue;
                    }

                }


                itemsData << uint32(slot + 1);        // client expects counting to start at 1
                itemsData << uint32(itemTemplate->MaxDurability);

                if (vendorItem->ExtendedCost)
                {
                    enablers.push_back(0);
                    itemsData << uint32(vendorItem->ExtendedCost);
                }
                else
                    enablers.push_back(1);
                enablers.push_back(1);                 // item is unlocked

                itemsData << uint32(vendorItem->item);
                itemsData << uint32(vendorItem->Type);     // 1 is items, 2 is currency
                itemsData << uint32(0);
                itemsData << uint32(itemTemplate->DisplayInfoID);
                itemsData << int32(leftInStock);
                itemsData << uint32(itemTemplate->BuyCount);

                if (++count >= MAX_VENDOR_ITEMS)
                    break;
            }
            else if (vendorItem->Type == ITEM_VENDOR_TYPE_CURRENCY)
            {
                CurrencyTypesEntry const* currencyTemplate = sCurrencyTypesStore.LookupEntry(vendorItem->item);
                if (!currencyTemplate)
                    continue;

                if (!vendorItem->ExtendedCost)
                    continue; // there's no price defined for currencies, only extendedcost is used

                itemsData << uint32(slot + 1);             // client expects counting to start at 1
                itemsData << uint32(0);                  // max durability

                enablers.push_back(0);
                itemsData << uint32(vendorItem->ExtendedCost);

                enablers.push_back(1);                    // item is unlocked

                itemsData << uint32(vendorItem->item);
                itemsData << uint32(vendorItem->Type);    // 1 is items, 2 is currency
                itemsData << uint32(0);                   // price, only seen currency types that have Extended cost
                itemsData << uint32(0);                   // displayId
                itemsData << int32(-1);
                itemsData << uint32(vendorItem->maxcount);

                if (++count >= MAX_VENDOR_ITEMS)
                    break;
            }
            // else error
        }

        ObjectGuid guid = item->GetGUID();

        WorldPacket data(SMSG_LIST_INVENTORY, 12 + itemsData.size());

        data.WriteBit(guid[1]);
        data.WriteBit(guid[0]);

        data.WriteBits(count, 21); // item count

        data.WriteBit(guid[3]);
        data.WriteBit(guid[6]);
        data.WriteBit(guid[5]);
        data.WriteBit(guid[2]);
        data.WriteBit(guid[7]);

        for (std::vector<bool>::const_iterator itr = enablers.begin(); itr != enablers.end(); ++itr)
            data.WriteBit(*itr);

        data.WriteBit(guid[4]);

        data.FlushBits();
        data.append(itemsData);

        data.WriteByteSeq(guid[5]);
        data.WriteByteSeq(guid[4]);
        data.WriteByteSeq(guid[1]);
        data.WriteByteSeq(guid[0]);
        data.WriteByteSeq(guid[6]);

        if (rawItemCount)
            data << uint8(rawItemCount);
        else
            data << uint8(0);

        data.WriteByteSeq(guid[2]);
        data.WriteByteSeq(guid[3]);
        data.WriteByteSeq(guid[7]);

        session->SendPacket(&data);
    }

    std::string ClassToString(uint16 class_id)
    {
        const char* classStr = nullptr;
        switch (class_id)
        {
            //warrior
        case CLASS_WARRIOR: classStr = "|TInterface\\ICONS\\inv_sword_27:35:35:-22:0|t";
            break;
            //paladin
        case CLASS_PALADIN: classStr = "|TInterface\\ICONS\\ability_thunderbolt:35:35:-22:0|t";
            break;
            //hunter
        case CLASS_HUNTER: classStr = "|TInterface\\ICONS\\inv_weapon_bow_07:35:35:-22:0|t";
            break;
            //rogue
        case CLASS_ROGUE: classStr = "|TInterface\\ICONS\\inv_throwingknife_04:35:35:-22:0|t";
            break;
            //priest
        case CLASS_PRIEST: classStr = "|TInterface\\ICONS\\inv_staff_30:35:35:-22:0|t";
            break;
            //Deathknight
        case CLASS_DEATH_KNIGHT: classStr = "|TInterface\\ICONS\\spell_deathknight_classicon:35:35:-22:0|t";
            break;
            //Shaman
        case CLASS_SHAMAN: classStr = "|TInterface\\ICONS\\spell_nature_bloodlust:35:35:-22:0|t";
            break;
            //mage
        case CLASS_MAGE: classStr = "|TInterface\\ICONS\\inv_staff_13.jpg:35:35:-22:0|t";
            break;
            //Warlock
        case CLASS_WARLOCK: classStr = "|TInterface\\ICONS\\spell_nature_drowsy:35:35:-22:0|t";
            break;
            //Druid
        case CLASS_DRUID: classStr = "|TInterface\\ICONS\\Ability_Druid_Maul:35:35:-22:0|t";
            break;
        default: classStr = "|TInterface\\ICONS\\Inv_misc_questionmark:35:35:-22:0|t";
            break;
        }
        return classStr;
    }

    void PrepareSuffix(Player* player, Item* itemPtr, int32 action, bool suffix)
    {
        if (!action)
            return;

        if (suffix)
            action = -action;

        PlayerStages[player->GetGUIDLow()].suffixId = action;
        if (!PrepareEnchantList(player, itemPtr, sObjectMgr->GetItemTemplate(PlayerStages[player->GetGUIDLow()].vendorItem->item)))
            Checkout(player);
    }

    void QueueSkirmish(Player* player)
    {
        Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(BATTLEGROUND_AA);
        if (!bg)
        {
            sLog->outError(LOG_FILTER_NETWORKIO, "Battleground: template bg (all arenas) not found");
            return;
        }

        BattlegroundTypeId bgTypeId = bg->GetTypeID();
        BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(bgTypeId, ARENA_TYPE_2v2);
        PvPDifficultyEntry const* bracketEntry = GetBattlegroundBracketByLevel(bg->GetMapId(), player->getLevel());
        if (!bracketEntry)
            return;

        GroupJoinBattlegroundResult err = ERR_BATTLEGROUND_NONE;

        Group* grp = player->GetGroup();
        // no group found, error
        if (!grp)
        {
            // check if already in queue
            if (player->GetBattlegroundQueueIndex(bgQueueTypeId) < PLAYER_MAX_BATTLEGROUND_QUEUES)
                //player is already in this queue
                return;
            // check if has free queue slots
            if (!player->HasFreeBattlegroundQueueId())
                return;

            BattlegroundQueue &bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId);

            GroupQueueInfo* ginfo = bgQueue.AddGroup(player, nullptr, bgTypeId, bracketEntry, ARENA_TEAM_2v2, false, false, 0, 0);
            uint32 avgTime = bgQueue.GetAverageQueueWaitTime(ginfo, bracketEntry->GetBracketId());
            uint32 queueSlot = player->AddBattlegroundQueueId(bgQueueTypeId);

            WorldPacket data;
            // send status packet (in queue)
            sBattlegroundMgr->BuildBattlegroundStatusPacket(&data, bg, player, queueSlot, STATUS_WAIT_QUEUE, avgTime, ginfo->JoinTime, ARENA_TEAM_2v2);
            player->GetSession()->SendPacket(&data);
            sBattlegroundMgr->ScheduleQueueUpdate(0, ARENA_TEAM_2v2, bgQueueTypeId, bgTypeId, bracketEntry->GetBracketId());
        }
        else
        {
            if (grp->GetLeaderGUID() != player->GetGUID())
            {
                ChatHandler(player->GetSession()).SendSysMessage("You are not the group leader.");
                return;
            }

            BattlegroundQueue &bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId);

            uint32 avgTime = 0;
            GroupQueueInfo* ginfo = NULL;

            err = grp->CanJoinBattlegroundQueue(bg, bgQueueTypeId, ARENA_TEAM_2v2, ARENA_TEAM_2v2, false, 0);
            if (!err)
            {
                ginfo = bgQueue.AddGroup(player, grp, bgTypeId, bracketEntry, ARENA_TEAM_2v2, false, false, 0, 0);
                avgTime = bgQueue.GetAverageQueueWaitTime(ginfo, bracketEntry->GetBracketId());
            }

            for (GroupReference* itr = grp->GetFirstMember(); itr != NULL; itr = itr->next())
            {
                Player* member = itr->getSource();
                if (!member)
                    continue;

                if (err)
                {
                    WorldPacket data;
                    sBattlegroundMgr->BuildStatusFailedPacket(&data, bg, player, 0, err);
                    member->GetSession()->SendPacket(&data);
                    continue;
                }

                // add to queue
                uint32 queueSlot = member->AddBattlegroundQueueId(bgQueueTypeId);

                // add joined time data
                member->AddBattlegroundQueueJoinTime(bgTypeId, ginfo->JoinTime);

                WorldPacket data; // send status packet (in queue)
                sBattlegroundMgr->BuildBattlegroundStatusPacket(&data, bg, member, queueSlot, STATUS_WAIT_QUEUE, avgTime, ginfo->JoinTime, ARENA_TEAM_2v2);
                member->GetSession()->SendPacket(&data);
            }

            sBattlegroundMgr->ScheduleQueueUpdate(0, ARENA_TEAM_2v2, bgQueueTypeId, bgTypeId, bracketEntry->GetBracketId());
        }
    }


	void OnGossipSelect(Player* player, Item* item, uint32 sender, uint32 action) 
	{
		player->PlayerTalkClass->ClearMenus();
        std::string text = "Greetings $N";

        WorldSession* sess = player->GetSession();
        if (sender == GOSSIP_SENDER_MAIN)
        {
            switch (action)
            {
            case 5000000:
                player->PlayerTalkClass->SendCloseGossip();
                break;

            case 1000:
            {
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Queue RBGs", GOSSIP_SENDER_MAIN, 10100);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, "RBG Ladder", GOSSIP_SENDER_MAIN, 10200);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, "Back to main menu", GOSSIP_SENDER_MAIN, 10300);
            } break;

            case 10100:
            {
                if (!player->GetGroup() || player->GetGroup()->GetLeaderGUID() != player->GetGUID())
                {
                    sess->SendNotification("You need to be in a group and be the group leader to be able to queue for Rated Battlegrounds.");
                    MainMenu(player, item);
                    return;
                }

                Group* group = player->GetGroup();

                if (group->GetMembersCount() != 10)
                {
                    sess->SendNotification("Your group needs to consist of 10 members.");
                    MainMenu(player, item);
                    return;
                }

                std::unordered_map<uint32, uint32> classCount;

                for (const GroupReference* itr = group->GetFirstMember(); itr != nullptr; itr = itr->next())
                {
                    if (Player* player = itr->getSource())
                        classCount[player->getClass()]++;
                }

                for (std::unordered_map<uint32, uint32>::iterator itr = classCount.begin(); itr != classCount.end(); ++itr)
                {
                    if (itr->second > 2)
                    {
                        std::string className = "N/A";
                        const ChrClassesEntry* classPlayer = sChrClassesStore.LookupEntry(itr->first);
                        if (classPlayer)
                            className = classPlayer->name;

                        sess->SendNotification("Your group can only contain 2 or less players with the class %s", className.c_str());
                        MainMenu(player, item);
                        return;
                    }
                }

                WorldPacket pack;
                player->GetSession()->HandleBattlemasterJoinRatedOpcode(pack);
            } break;

            case 10200:
            {
                QueryResult result = CharacterDatabase.Query("SELECT * FROM character_battleground_stats ORDER BY rating DESC LIMIT 10");
                uint32 rank = 1;
                if (result)
                {
                    do
                    {
                        Field* fields = result->Fetch();
                        uint32 lowGuid = fields[0].GetUInt32();
                        uint16 won = fields[1].GetUInt16();
                        uint16 played = fields[2].GetUInt16();
                        uint16 rating = fields[3].GetUInt16();
                        QueryResult charName = CharacterDatabase.PQuery("SELECT class, name FROM characters WHERE guid = %u", lowGuid);
                        float percentageWins = 0.0f;
                        if ((played - won) + won > 0)
                            percentageWins = (float)((float)won / (float)(won + (played - won))) * 100.0f;

                        std::ostringstream ss;
                        if (charName)
                        {
                            ss << ClassToString((*charName).Fetch()[0].GetUInt8()) << " " << charName->Fetch()[1].GetString() << ":" << rating << ":" <<
                                won << " W:" << played - won << " L:" << percentageWins << "%";
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, ss.str().c_str(), GOSSIP_SENDER_MAIN, 5000);
                        }
                    } while (result->NextRow());
                }
                text = "Here are the top 10 Rated players.";
            } break;

            case 10300:
            {
                MainMenu(player, item);
                return;
            } break;


            case 1100:
            case 1200:
            {
                if (action == 1200)
                    PlayerStages[player->GetGUIDLow()].vendorStage = VENDOR_STAGE_PRESELECT_RBG;
                SendCustomListInventory(player->GetSession(), VENDOR_ID_METADATA, item);
            } break;

            case 1300:
            {
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Faction Change", GOSSIP_SENDER_CHANGER, AT_LOGIN_CHANGE_FACTION);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Race Change", GOSSIP_SENDER_CHANGER, AT_LOGIN_CHANGE_RACE);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Customize", GOSSIP_SENDER_CHANGER, AT_LOGIN_CUSTOMIZE);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Reset Talents", GOSSIP_SENDER_CHANGER, 1337);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Professions", GOSSIP_SENDER_PROFF, 1);
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Main Menu", GOSSIP_SENDER_MAIN, 5000);
                text = "Pick any changes you want to make.\nPlease note that you will be disconnected after any option except \'Reset Talents\' and \'Professions\'";
            }break;

            case 1400:
            {
                uint32 arrSize = sizeof(petEntries) / sizeof(*petEntries);
                for (uint32 i = 0; i < arrSize; ++i)
                {
                    CreatureTemplate const* creatureTemplate = sObjectMgr->GetCreatureTemplate(petEntries[i].first);
                    if (!creatureTemplate)
                        continue;

                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, petEntries[i].second, GOSSIP_SENDER_PET, petEntries[i].first);
                }

                text = "Pick your pet.";
            }break;

            case 1500:
            {
                QueueSkirmish(player);
            }break;

            default:
            {
                    if (PlayerStages[player->GetGUIDLow()].vendorStage == VENDOR_STAGE_FAKE_ENCHANT)
                    {
                        PlayerStages[player->GetGUIDLow()].vendorStage = VENDOR_STAGE_ENCHANT;
                    }
                    else
                    {
                        MainMenu(player, item);
                        return;
                    }
            } break;
            }
        }

        else if (sender == GOSSIP_SENDER_CHANGER)
        {
            player->PlayerTalkClass->SendCloseGossip();
            if (action == 1337)
            {
                player->ResetTalents(true);
                player->SendTalentsInfoData(false);
            }
            else
            {
                player->SetAtLoginFlag((AtLoginFlags)action);
                player->GetSession()->KickPlayer("Customize");
            }
        }

        else if (sender == GOSSIP_SENDER_PET)
        {
            CreatureTemplate const* creatureTemplate = sObjectMgr->GetCreatureTemplate(action);
            if (creatureTemplate && player->getClass() == CLASS_HUNTER)
            {
                TamePet(player, action);
                player->PlayerTalkClass->SendCloseGossip();
            }
        }

        else if ((sender == GOSSIP_SENDER_SUFFIX || sender == GOSSIP_SENDER_RANDPROP) && PlayerStages[player->GetGUIDLow()].vendorStage == VENDOR_STAGE_PREFIX)
            PrepareSuffix(player, item, action, sender == GOSSIP_SENDER_SUFFIX);

        else if (sender == GOSSIP_SENDER_PROFF)
        {
            if (action == 1)
            {
                if (!player->HasSkill(SKILL_HERBALISM))
                {
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Herbalism", GOSSIP_SENDER_PROFF, SKILL_HERBALISM);
                }
                if (!player->HasSkill(SKILL_SKINNING))
                {
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Skinning", GOSSIP_SENDER_PROFF, SKILL_SKINNING);
                }
                if (!player->HasSkill(SKILL_MINING))
                {
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Mining", GOSSIP_SENDER_PROFF, SKILL_MINING);
                }
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Unlearn All", GOSSIP_SENDER_PROFF, 0);
            }
            else if (!action)
            {
                ForgotSkill(player, SKILL_ENGINEERING);
                ForgotSkill(player, SKILL_HERBALISM);
                ForgotSkill(player, SKILL_SKINNING);
                ForgotSkill(player, SKILL_MINING);
                OnGossipSelect(player, item, GOSSIP_SENDER_PROFF, 1);
                    return;
            }
            else
            {
                if (player->HasSkill(action))
                    player->GetSession()->SendNotification(TXT_ERR_SKILL);
                else
                {
                    CompleteLearnProfession(player, (SkillType)action);
                    OnGossipSelect(player, item, GOSSIP_SENDER_PROFF, 1);
                    return;
                }
            }
        }

        player->PlayerTalkClass->SendCloseGossip();
        if (!player->PlayerTalkClass->GetGossipMenu().GetMenuItems().empty())
        {
            player->PlayerTalkClass->SendGossipMenu(text, item->GetGUID());
        }
	}

	bool OnUse(Player* player, Item* item, SpellCastTargets const& targets) 
	{ 
        PlayerStages[player->GetGUIDLow()] = GossipStageInfo();
		MainMenu(player, item);
		return true; 
	}

	void MainMenu(Player* player, Item* item)
	{
        player->PlayerTalkClass->SendCloseGossip();
		player->PlayerTalkClass->ClearMenus();
        PlayerStages[player->GetGUIDLow()].vendorStage = VENDOR_STAGE_PRESELECT;
        PlayerStages[player->GetGUIDLow()].gossipItemGuid = item->GetGUID();

        InfoCharEntry info;
        bool hasInfo = sInfoMgr->GetCharInfo(GUID_LOPART(player->GetGUID()), info);

        if (hasInfo)
        {
            if (!info.RbgPlayed)
                sInfoMgr->UpdateCharRBGstats(info.Guid, 0, 0, 0);
        }

		uint32 rbgRating = hasInfo ? info.RBGRating : 0;
	if (player->GetTeamId() != TEAM_ALLIANCE)
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "|TInterface\\Icons\\inv_bannerpvp_02:20:20:-21:0|tRated Battlegrounds", GOSSIP_SENDER_MAIN, 1000);
    else
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "|TInterface\\Icons\\inv_bannerpvp_01:20:20:-21:0|tRated Battlegrounds", GOSSIP_SENDER_MAIN, 1000);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, "|TInterface\\Icons\\inv_helmet_31:20:20:-21:0|tGear", GOSSIP_SENDER_MAIN, 1100);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, "|TInterface\\Icons\\inv_chest_cloth_25:20:20:-21:0|tGF\'d Gear", GOSSIP_SENDER_MAIN, 1200);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "|Tinterface\\icons\\inv_misc_gear_01:20:20:-21:0|tCharacter Setup", GOSSIP_SENDER_MAIN, 1300);
        if (player->getClass() == CLASS_HUNTER)
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "|Tinterface\\icons\\ability_hunter_pet_raptor:20:20:-21:0|tPets", GOSSIP_SENDER_MAIN, 1400);

        if (sWorld->GetPlayerCount() < 20)
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "|Tinterface\\icons\\Achievement_Arena_2v2_7:20:20:-21:0|tQueue Skirmish", GOSSIP_SENDER_MAIN, 1500);
		std::ostringstream ss;
        uint32 amountOfWins = hasInfo ? info.RbgWon : 0;

        int32 losses = hasInfo ? (int32)info.RbgPlayed - (int32)info.RbgWon : 0;

        uint32 amountOfLosses = hasInfo ? losses : 0;

        float percentageWins = 0.0f;
        if (amountOfLosses + amountOfWins > 0)
            percentageWins = (float)((float)amountOfWins / (float)(amountOfWins + amountOfLosses)) * 100.0f;

        if (hasInfo && info.RbgPlayed > 0)
            ss << "You have " << rbgRating << " RBG rating - " << amountOfWins << "W / " << amountOfLosses << "L : " << percentageWins << "%.";
        else
            ss << "You should play some RBG games.";

		player->PlayerTalkClass->SendGossipMenu(ss.str(), item->GetGUID());
	}
};

twink_tablet* tablet = nullptr;


void AddSC_twink_tablet()
{
    tablet = new twink_tablet;
    new twink_tablet_loader;
}


void BuyError(Player* player, BuyResult msg, uint32 item)
{
    WorldPacket data(SMSG_BUY_FAILED, (8 + 4 + 4 + 1));
    data << uint64(0);
    data << uint32(item);
    data << uint8(msg);
    player->GetSession()->SendPacket(&data);
}

bool PrepareEnchantList(Player* player, Item* itemPtr, const ItemTemplate* proto)
{
    if (!player || !itemPtr || !proto)
        return false;

    player->PlayerTalkClass->ClearMenus();
    
    std::pair<EnchantMap::iterator, EnchantMap::iterator> range;
    range = EnchantIds.equal_range(proto->InventoryType);
    if (range.first == range.second || range.first == EnchantIds.end())
        return false;

    //populate our list now.
    uint32 rawItemCount = std::distance(range.first, range.second);
    VendorItemList vendorItems;
    std::vector<VendorItem> items;
    EnchantMap::iterator itr = range.first;

    for (uint32 i = 0; i < rawItemCount; ++i)
    {
        items.emplace_back(itr->second + INCREMENT_ENCHANT_ID, 0, 0, 0, 1, 0);
        ++itr;
    }

    for (uint32 i = 0; i < rawItemCount; ++i)
    {
        vendorItems.push_back(&items[i]);
    }

    if (rawItemCount != 0)
        PlayerStages[player->GetGUIDLow()].vendorStage = VENDOR_STAGE_FAKE_ENCHANT;
    else
        return false;

    //and send it off..
    tablet->SendCustomListInventory(player->GetSession(), 0, itemPtr, &vendorItems);
    return true;
}

bool PrepareSuffixList(Player* player, Item* itemPtr)
{
    uint32 entry = PlayerStages[player->GetGUIDLow()].vendorItem->item;
    player->m_Events.AddEvent(new SendSuffixGossip(player, itemPtr, entry), player->m_Events.CalculateTime(200));

    std::pair<std::multimap<uint32 /*item entry*/, int32 /*suffixId*/>::iterator, 
        std::multimap<uint32 /*item entry*/, int32 /*suffixId*/>::iterator> range = ItemSuffixes.equal_range(entry);

    if (range.first == range.second)
        return false;
    player->PlayerTalkClass->SendGossipMenu(907, itemPtr->GetGUID());
    player->PlayerTalkClass->SendCloseGossip();
    return true;
}

void HandleEnchantSelect(Player* player, uint32 action)
{
    if (PlayerStages[player->GetGUIDLow()].vendorStage == VENDOR_STAGE_ENCHANT || PlayerStages[player->GetGUIDLow()].vendorStage == VENDOR_STAGE_FAKE_ENCHANT)
    {
        for (EnchantMap::iterator itr = EnchantIds.begin(); itr != EnchantIds.end(); ++itr)
        {
            if (itr->second == action) // enchant..
            {
                PlayerStages[player->GetGUIDLow()].enchantId = action;
                Checkout(player);
                return;
            }
        }
        player->PlayerTalkClass->SendCloseGossip();
    }
}

bool CustomStoreItem(Player* player, uint32 vendorslot, uint32 item, uint8 count, uint8 bag, uint8 slot, int32 price, ItemTemplate const* pProto, Creature* pVendor, VendorItem const* crItem, bool bStore, uint64& itemGuid)
{
    uint32 stacks = count / pProto->BuyCount;
    ItemPosCountVec vDest;
    uint16 uiDest = 0;
    InventoryResult msg = bStore ?
        player->CanStoreNewItem(bag, slot, vDest, item, count) :
        player->CanEquipNewItem(slot, uiDest, item, false);
    if (msg != EQUIP_ERR_OK)
    {
        player->SendEquipError(msg, nullptr, nullptr, item);
        return false;
    }

    player->ModifyMoney(-price);

    if (crItem->ExtendedCost) // case for new honor system
    {
        ItemExtendedCostEntry const* iece = sItemExtendedCostStore.LookupEntry(crItem->ExtendedCost);
        for (int i = 0; i < MAX_ITEM_EXT_COST_CURRENCIES; ++i)
        {
            if (iece->RequiredItem[i])
                player->DestroyItemCount(iece->RequiredItem[i], iece->RequiredItemCount[i] * stacks, true);
        }

        for (int i = 0; i < MAX_ITEM_EXT_COST_CURRENCIES; ++i)
        {
            CurrencyTypesEntry const* entry = sCurrencyTypesStore.LookupEntry(iece->RequiredCurrency[i]);
            if (!entry)
                continue;

            // Skip check only currencies.. 
            uint32 checkValue = entry->TotalCap;
            if (entry->ID == CURRENCY_TYPE_CONQUEST_POINTS)
                checkValue = 600000;

            if (checkValue > 0 && iece->RequiredCurrencyCount[i] * stacks > checkValue)
                continue;

            if (iece->RequiredCurrency[i])
                player->ModifyCurrency(iece->RequiredCurrency[i], -int32(iece->RequiredCurrencyCount[i] * stacks), true, true);
        }
    }

    Item* it = bStore ?
        player->StoreNewItem(vDest, item, true) :
        player->EquipNewItem(uiDest, item, true);
    if (it)
    {
        uint32 new_count = pVendor ? pVendor->UpdateVendorItemCurrentCount(crItem, count) : 0;

        WorldPacket data(SMSG_BUY_ITEM, (8 + 4 + 4 + 4));
        data << uint64(pVendor ? pVendor->GetGUID() : 0);
        data << uint32(vendorslot + 1);                   // numbered from 1 at client
        data << int32(crItem->maxcount > 0 ? new_count : 0xFFFFFFFF);
        data << uint32(count);
        player->GetSession()->SendPacket(&data);
        player->SendNewItem(it, count, true, false, false);

        if (!bStore)
            player->AutoUnequipOffhandIfNeed();

        if (pProto->Flags & ITEM_PROTO_FLAG_REFUNDABLE && crItem->ExtendedCost && pProto->GetMaxStackSize() == 1)
        {
            it->SetFlag(ITEM_FIELD_FLAGS, ITEM_FLAG_REFUNDABLE);
            it->SetRefundRecipient(player->GetGUIDLow());
            it->SetPaidMoney(price);
            it->SetPaidExtendedCost(crItem->ExtendedCost);
            it->SaveRefundDataToDB();
            player->AddRefundReference(it->GetGUIDLow());
        }
        itemGuid = it->GetGUID();
    }
    return true;
}

void HandlePreSelectCallback(Player* player, uint64 vendorguid, uint32 vendorslot, uint32 item, uint8 count, uint8 bag, uint8 slot)
{
    if (PlayerStages[player->GetGUIDLow()].vendorStage == VENDOR_STAGE_PRESELECT || PlayerStages[player->GetGUIDLow()].vendorStage == VENDOR_STAGE_PRESELECT_RBG)
    {
        ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(item);

        if (!pProto->ScriptId)
            return;

        
        if (SlotVendorInfo.find(sObjectMgr->GetScriptName(pProto->ScriptId)) == SlotVendorInfo.end())
            return;

        Item* itemPtr = player->GetItemByGuid(vendorguid);
        PlayerStages[player->GetGUIDLow()].metaItemName = sObjectMgr->GetScriptName(pProto->ScriptId);

        if (itemPtr)
        {
            tablet->SendCustomListInventory(player->GetSession(),
                PlayerStages[player->GetGUIDLow()].vendorStage == VENDOR_STAGE_PRESELECT ? SlotVendorInfo[sObjectMgr->GetScriptName(pProto->ScriptId)].first :
                SlotVendorInfo[sObjectMgr->GetScriptName(pProto->ScriptId)].second,
                itemPtr, nullptr, PlayerStages[player->GetGUIDLow()].vendorStage == VENDOR_STAGE_PRESELECT_RBG ? true : false);

            PlayerStages[player->GetGUIDLow()].RBG = PlayerStages[player->GetGUIDLow()].vendorStage == VENDOR_STAGE_PRESELECT_RBG ? true : false;
            PlayerStages[player->GetGUIDLow()].vendorStage = VENDOR_STAGE_BUY;
        }
    }
    else if (PlayerStages[player->GetGUIDLow()].vendorStage == VENDOR_STAGE_FAKE_ENCHANT || PlayerStages[player->GetGUIDLow()].vendorStage == VENDOR_STAGE_ENCHANT)
    {
        uint32 enchantId = item - INCREMENT_ENCHANT_ID;

        HandleEnchantSelect(player, enchantId);
    }
    else if (PlayerStages[player->GetGUIDLow()].vendorStage == VENDOR_STAGE_BUY)
    {
        ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(item);

        VendorItemData const* vItems = sObjectMgr->GetNpcVendorItemList(PlayerStages[player->GetGUIDLow()].RBG ? SlotVendorInfo[PlayerStages[player->GetGUIDLow()].metaItemName].second :
            SlotVendorInfo[PlayerStages[player->GetGUIDLow()].metaItemName].first);

        VendorItemList sortedItems;
        if (PlayerStages[player->GetGUIDLow()].RBG)
        {
            sortedItems = vItems->m_items;
            std::sort(sortedItems.begin(), sortedItems.end(), RbgItem_Sort());
        }
        else
        {
            sortedItems = vItems->m_items;
            VendorItem const* vendorItem = (sortedItems)[0];
            if (!vendorItem)
                return;

            ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(vendorItem->item);
            if (!itemTemplate)
                return;

            if (itemTemplate->Class == ITEM_CLASS_ARMOR && itemTemplate->SubClass < ITEM_SUBCLASS_ARMOR_BUCKLER)
                std::sort(sortedItems.begin(), sortedItems.end(), [](const VendorItem* item1, const VendorItem* item2) {
                ItemTemplate const* item1Temp = sObjectMgr->GetItemTemplate(item1->item);
                if (!item1Temp)
                    return true;

                ItemTemplate const* item2Temp = sObjectMgr->GetItemTemplate(item2->item);
                if (!item2Temp)
                    return true;

                return std::greater<uint32>()(item1Temp->SubClass, item2Temp->SubClass);
            });

        }

        Item* itemPtr = player->GetItemByGuid(vendorguid);

        if (!vItems || vItems->Empty())
        {
            BuyError(player, BUY_ERR_CANT_FIND_ITEM, item);
            return;
        }

        if (vendorslot >= vItems->GetItemCount())
        {
            BuyError(player, BUY_ERR_CANT_FIND_ITEM, item);
            return;
        }


        VendorItem const* crItem = sortedItems[vendorslot];
        // store diff item (cheating)
        if ((!crItem || crItem->item != item))
        {
            BuyError(player, BUY_ERR_CANT_FIND_ITEM, item);
            return;
        }

        if (RatingItems[pProto->ItemId] > player->GetRBGPersonalRating())
        {
            player->GetSession()->SendNotification("You need %u RBG personal rating for this item.", RatingItems[pProto->ItemId]);
            return;
        }

        uint32 questItemCount = player->GetItemCount(crItem->item, true);

        if (!player->isGameMaster() && crItem->QuestItem && questItemCount >= crItem->QuestItem)
        {
            player->GetSession()->SendNotification("You can't have more of this item.");
            return;
        }

        if (pProto->RequiredReputationFaction && (uint32(player->GetReputationRank(pProto->RequiredReputationFaction)) < pProto->RequiredReputationRank))
        {
            BuyError(player, BUY_ERR_REPUTATION_REQUIRE, item);
            return;
        }

        if (crItem->ExtendedCost)
        {
            // Can only buy full stacks for extended cost
            if (count % pProto->BuyCount)
            {
                player->SendEquipError(EQUIP_ERR_CANT_BUY_QUANTITY, nullptr, nullptr);
                return;
            }

            uint32 stacks = count / pProto->BuyCount;
            ItemExtendedCostEntry const* iece = sItemExtendedCostStore.LookupEntry(crItem->ExtendedCost);
            if (!iece)
            {
                sLog->outError(LOG_FILTER_PLAYER, "Item %u have wrong ExtendedCost field value %u", pProto->ItemId, crItem->ExtendedCost);
                return;
            }

            for (uint8 i = 0; i < MAX_ITEM_EXT_COST_ITEMS; ++i)
            {
                if (iece->RequiredItem[i] && !player->HasItemCount(iece->RequiredItem[i], iece->RequiredItemCount[i] * stacks))
                {
                    player->SendEquipError(EQUIP_ERR_VENDOR_MISSING_TURNINS, nullptr, nullptr);
                    return;
                }
            }

            for (uint8 i = 0; i < MAX_ITEM_EXT_COST_CURRENCIES; ++i)
            {
                if (!iece->RequiredCurrency[i])
                    continue;

                CurrencyTypesEntry const* entry = sCurrencyTypesStore.LookupEntry(iece->RequiredCurrency[i]);
                if (!entry)
                {
                    BuyError(player, BUY_ERR_CANT_FIND_ITEM, item);
                    return;
                }

                // See if we mush check CurrentAmount or TotalSeasonAmount
                uint32 checkValue = entry->TotalCap;
                if (entry->ID == CURRENCY_TYPE_CONQUEST_POINTS)
                    checkValue = 600000;

                if (checkValue != 0 && iece->RequiredCurrencyCount[i] * stacks > checkValue)
                {
                    // Check for total season amount
                    if (!player->HasCurrencySeasonCount(iece->RequiredCurrency[i], iece->RequiredCurrencyCount[i] * stacks))
                    {
                        player->SendEquipError(EQUIP_ERR_VENDOR_MISSING_TURNINS, nullptr, nullptr);
                        return;
                    }
                }
                else
                {
                    // Check for current amount
                    if (!player->HasCurrency(iece->RequiredCurrency[i], iece->RequiredCurrencyCount[i] * stacks))
                    {
                        player->SendEquipError(EQUIP_ERR_VENDOR_MISSING_TURNINS, nullptr, nullptr);
                        return;
                    }
                }
            }

            // check for personal arena rating requirement
            if (player->GetMaxPersonalArenaRatingRequirement(iece->RequiredArenaSlot) < iece->RequiredPersonalArenaRating)
            {
                // probably not the proper equip err
                player->SendEquipError(EQUIP_ERR_CANT_EQUIP_RANK, nullptr, nullptr);
                return;
            }

            if (iece->RequiredFactionId && player->GetReputationRank(iece->RequiredFactionId) << iece->RequiredFactionStanding)
            {
                BuyError(player, BUY_ERR_REPUTATION_REQUIRE, item);
                return;
            }

            if (iece->RequirementFlags & ITEM_EXT_COST_FLAG_REQUIRE_GUILD && !player->GetGuildId())
            {
                player->SendEquipError(EQUIP_ERR_VENDOR_MISSING_TURNINS, nullptr, nullptr); // Find correct error
                return;
            }

            if (iece->RequiredGuildLevel && iece->RequiredGuildLevel < player->GetGuildLevel())
            {
                player->SendEquipError(EQUIP_ERR_VENDOR_MISSING_TURNINS, nullptr, nullptr); // Find correct error
                return;
            }

            if (iece->RequiredAchievement && !player->HasAchieved(iece->RequiredAchievement))
            {
                player->SendEquipError(EQUIP_ERR_VENDOR_MISSING_TURNINS, nullptr, nullptr); // Find correct error
                return;
            }
        }
        PlayerStages[player->GetGUIDLow()].vendorItem = crItem;
        PlayerStages[player->GetGUIDLow()].vendorSlot = vendorslot;

        bool checkout = false;

        if (pProto->InventoryType == INVTYPE_LEGS)
        {
            if (pProto->Bonding == BIND_WHEN_EQUIPED)
            {
                PlayerStages[player->GetGUIDLow()].enchantId = 17;
                if (!PrepareSuffixList(player, itemPtr))
                    checkout = true;
            }
            else if (pProto->Bonding == BIND_WHEN_PICKED_UP)
            {
                PlayerStages[player->GetGUIDLow()].enchantId = 16;
                if (!PrepareSuffixList(player, itemPtr))
                    checkout = true;
            }
        }
        else if (pProto->InventoryType == INVTYPE_RANGED)
        {
            PlayerStages[player->GetGUIDLow()].enchantId = 32;
            checkout = true;
        }
        else
        {
            if (!PrepareSuffixList(player, itemPtr))
            {
                if (!PrepareEnchantList(player, itemPtr, pProto))
                    checkout = true;
            }
        }

        if (checkout)
            Checkout(player);
    }
}

//nice function name.
void Checkout(Player* player)
{
    uint32 price = 0;
    uint64 newItemGuid = 0;
    auto crItem = PlayerStages[player->GetGUIDLow()].vendorItem;
    uint32 vendorslot = PlayerStages[player->GetGUIDLow()].vendorSlot;
    uint32 enchantId = PlayerStages[player->GetGUIDLow()].enchantId;
    uint32 suffixId = PlayerStages[player->GetGUIDLow()].suffixId;

    auto pProto = sObjectMgr->GetItemTemplate(crItem->item);

    if (!CustomStoreItem(player, vendorslot, crItem->item, 1, NULL_BAG, NULL_SLOT, price, pProto, nullptr, crItem, true, newItemGuid))
        return;


    if (crItem->maxcount != 0) // bought
    {
        if (pProto->Quality > ITEM_QUALITY_EPIC || (pProto->Quality == ITEM_QUALITY_EPIC))
            if (Guild* guild = player->GetGuild())
                guild->AddGuildNews(GUILD_NEWS_ITEM_PURCHASED, player->GetGUID(), 0, crItem->item);
    }

    PlayerStages[player->GetGUIDLow()].enchantItemGuid = newItemGuid;
    auto itmemem = player->GetItemByGuid(newItemGuid);

    Enchant(player, enchantId);
    itmemem->SetItemRandomProperties(PlayerStages[player->GetGUIDLow()].suffixId);

    if (auto itemptr = player->GetItemByGuid(PlayerStages[player->GetGUIDLow()].gossipItemGuid))
    {
        PlayerStages[player->GetGUIDLow()].vendorStage = PlayerStages[player->GetGUIDLow()].RBG ? VENDOR_STAGE_PRESELECT_RBG : VENDOR_STAGE_PRESELECT;
        PlayerStages[player->GetGUIDLow()].enchantId = 0;
        PlayerStages[player->GetGUIDLow()].enchantItemGuid = 0;
        PlayerStages[player->GetGUIDLow()].metaItemName = "";
        PlayerStages[player->GetGUIDLow()].suffixId = 0;
        PlayerStages[player->GetGUIDLow()].RBG = false;
        PlayerStages[player->GetGUIDLow()].vendorSlot = 0;
        tablet->SendCustomListInventory(player->GetSession(), VENDOR_ID_METADATA, itemptr);
    }
    else
        PlayerStages[player->GetGUIDLow()] = GossipStageInfo();
}