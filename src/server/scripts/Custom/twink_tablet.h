#include "ScriptPCH.h"
#include "InfoMgr.h"
#include "Group.h"
#include "Guild.h"
#include "Pet.h"

#include <unordered_map>
#include <map>
#include <vector>
#include <utility>
#include <array>
#include <sstream>

#define GOSSIP_SENDER_CHANGER 2
#define GOSSIP_SENDER_PET 3
#define GOSSIP_SENDER_SUFFIX 4
#define GOSSIP_SENDER_RANDPROP 5
#define GOSSIP_SENDER_PROFF 6
#define INCREMENT_ENCHANT_ID 500000

/*config*/
#define VENDOR_ID_METADATA 250000 // should be an entry in npc_vendor that sells "Shoulders", "Legs" etc..



void HandlePreSelectCallback(Player* player, uint64 vendorguid, uint32 vendorslot, uint32 item, uint8 count, uint8 bag, uint8 slot);
void HandleEnchantSelect(Player* player, uint32 action);

enum VendorStage
{
    VENDOR_STAGE_PRESELECT,
    VENDOR_STAGE_PRESELECT_RBG,
    VENDOR_STAGE_BUY,
    VENDOR_STAGE_FAKE_ENCHANT, // to trick first client packet, bugged.
    VENDOR_STAGE_ENCHANT,
    VENDOR_STAGE_PREFIX
};


struct GossipStageInfo
{
    GossipStageInfo() : vendorStage(VENDOR_STAGE_PRESELECT), enchantItemGuid(0), gossipItemGuid(0), RBG(false), suffixId(0), enchantId(0), vendorItem(nullptr), vendorSlot(0) {}
    uint64 enchantItemGuid;
    uint32 enchantId;
    int32 suffixId;
    uint32 vendorStage;
    std::string metaItemName;
    bool RBG;
    const VendorItem* vendorItem;
    uint32 vendorSlot;
    uint64 gossipItemGuid;
};

#define TXT_BAD_SKILL   "Profession NPC: received non-valid skill ID (LearnAllRecipesInProfession)"
#define TXT_ERR_SKILL   "you already have that skill"
#define TXT_PROBLEM     "Internal error occured!"
#define TXT_ERR_MAX     "you already know two professions!"

enum Gossip_Option_Custom
{
    CUSTOM_OPTION_NONE = 20,
    CUSTOM_OPTION_UNLEARN = 21,
    CUSTOM_OPTION_EXIT = 22,
    CUSTOM_OPTION_TITLE_PVP = 23,
    CUSTOM_OPTION_TITLE_CLIMB = 24,
    CUSTOM_OPTION_SPRINT = 25,
    GOSSIP_OPTION_HELLO = 26,
    CUSTOM_OPTION_ITEM_MENU = 27,
    CUSTOM_OPTION_ITEM_MENU_P2 = 28,
    CUSTOM_OPTION_ITEM_MENU_P3 = 29,
    CUSTOM_OPTION_ITEM_MENU_P4 = 30,
    CUSTOM_OPTION_ITEM_MENU_P5 = 31,
    CUSTOM_OPTION_ITEM_MENU_P6 = 32,
    CUSTOM_OPTION_ITEM_MENU_P7 = 33,
    CUSTOM_OPTION_ITEM_MENU_MAX = 34,
    CUSTOM_OPTION_SUFFIX = 35,
    CUSTOM_OPTION_PROPERTY = 36,
    CUSTOM_OPTION_VENDOR = 37,
    CUSTOM_OPTION_MAX
};
