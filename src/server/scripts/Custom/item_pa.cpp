//#include "Group.h"
//#include "Pet.h"
//
//const uint32 ItemQualityColorsDark[MAX_ITEM_QUALITY] =
//{
//	0xff9d9d9d,        //GREY
//	0xffffffff,        //WHITE
//	0xff0f7f00,        //GREEN
//	0xff004384,        //BLUE
//	0xffa335ee,        //PURPLE
//	0xffff8000,        //ORANGE
//	0xff473a15,        //LIGHT YELLOW
//	0xff473a15         //LIGHT YELLOW
//};
//
//const uint32 HunterPetIds[16] =
//{
//	16355,
//	2354,
//	1997,
//	1191,
//	14268,
//	3619,
//	3812,
//	5053,
//	15650,
//	5829,
//	17350,
//	3257,
//	17527,
//	3127,
//	5755,
//	4007
//};
//
//const uint32 statusColors[2] =
//{
//	0xffce0f0f,
//	0xff097b09
//};
//
//const uint32 profSpells[3] = { 55500, 53663, 53122 };
//
//const std::string statNames[49] =
//{
//	"Mana",
//	"Health",
//	"Agility",
//	"Strength",
//	"Intellect",
//	"Spirit",
//	"Stamina",
//	"Defense",
//	"Dodge Rating",
//	"Parry Rating",
//	"Block Rating",
//	"16",
//	"17",
//	"18",
//	"19",
//	"20",
//	"21",
//	"22",
//	"23",
//	"24",
//	"25",
//	"26",
//	"27",
//	"28",
//	"29",
//	"30",
//	"Hit Rating",
//	"Crit Rating",
//	"33",
//	"34",
//	"Resilience Rating",
//	"Haste Rating",
//	"Expertise Rating",
//	"Attack Power",
//	"Ranged Attack Power",
//	"40",
//	//ITEM_MOD_FERAL_ATTACK_POWER       = 40, not in 3.3
//	"41",                 // deprecated
//	"42",                // deprecated
//	"Mana Regeneration",
//	"Armor Penetration",
//	"Spell Power",
//	"Health Regeneration",
//	"Spell Penetration",
//	"Block Value"
//};
//
//class item_pa : public ItemScript
//{
//
//public:
//	item_pa() : ItemScript("item_pa"),
//		gearVendorEntry(ObjectGuid(uint64(27760))),
//		inventoryTypeVendorEntry(ObjectGuid(uint64(27761))),
//		glyphVendorEntry(ObjectGuid(uint64(50000)))
//		//paOptions({ { "Filter Gear (experimental)", "Detailed Item Info (WIP)" } }),
//	{ 
//		itemIconPrefix = "|cff00ff00|TInterface\\icons\\";
//		iconPrefix = "|cff00ff00|T";
//		iconSuffix = ":20:20:-21:0|t|r ";
//		templateIconSuffix = ":50:50:-25:0|t|r ";
//		
//		//paModeIcons[3] =  { "ayh", "suj", "dh" };
//		//inventoryTypes[29] = { "none", "Head", "Neck", "Shoulder", "Shirt", "Chest", "Waist", "Legs", "Feet", "Wrists", "Hands", "Finger", "Trinket", "Weapon", "Shield", "Ranged", "Back", "Two-Hand", "Bag", "Tabard", "Robe", "Mainhand", "Offhand", "Holdable", "Ammo", "Thrown", "Wands,Guns", "Quiver", "Relic" };
//	} // ItemScript("itemScriptTemplate") = Name of database entry
//
//	// Called when the player will use (rightclick) the item
//	// IMPORTANT: Item spellid_1 has to be ID 33227 and spellcharges_1 has to be -1 in the database.
//	// You also have to set the item to usable (flags 64)
//
//
//	ObjectGuid gearVendorEntry;
//	ObjectGuid inventoryTypeVendorEntry;
//	ObjectGuid glyphVendorEntry;
//	std::vector<SpellItemEnchantmentEntry const*> enchantList;
//
//	std::string itemIconPrefix;
//	std::string iconPrefix;
//	std::string iconSuffix;
//	std::string templateIconSuffix;
//
//	bool InitEnchantList(uint32 id, Player* player)
//	{
//		enchantList.clear();
//		ItemTemplate const* itemp = sObjectMgr->GetItemTemplate(id);
//		uint32 type = itemp->InventoryType;
//		uint32 level = itemp->ItemLevel;
//		std::vector<uint32> multitype;
//		multitype.push_back(type);
//		if (type == 26)
//			if (itemp->SubClass == 19)
//				return false;
//
//		if (type == 17 || type == 21 || type == 22)
//			multitype.push_back(13);
//
//		if (type == 20)
//			multitype.push_back(5);
//
//		uint32 failed = 0;
//		for (uint8 i = 0; i < multitype.size(); i++)
//		{
//			PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_ENCHANTIDS);
//			stmt->setUInt32(0, multitype[i]);
//			PreparedQueryResult result = WorldDatabase.Query(stmt);
//			if (!result)
//			{
//				failed++;
//				continue;
//			}
//			do
//			{
//				Field* fields = result->Fetch();
//				uint32 spellid = fields[0].GetUInt32();
//				SpellEffectEntry const* ppSpell = sSpellEffectStore.LookupEntry(spellid);
//				SpellLevelsEntry const* pSpell = sSpellLevelsStore.LookupEntry(spellid);
//				if (pSpell && pSpell->baseLevel > level)
//					continue;
//				SpellItemEnchantmentEntry const* pEnchant = sSpellItemEnchantmentStore.LookupEntry(ppSpell->EffectMiscValue);
//				enchantList.push_back(pEnchant);
//			} while (result->NextRow());
//		}
//		return failed != multitype.size();
//	}
//	
//	struct sort_enchant 
//	{
//		bool operator()(SpellItemEnchantmentEntry const* lhs, SpellItemEnchantmentEntry const* rhs)
//		{
//			return lhs->description[0] < rhs->description[0];
//		}
//	};
//
//	void GenerateEnchantList(Player* player)
//	{
//		/*player->_twinkTabletData.textId = 50003;
//		if (enchantList.empty())
//			return;
//		std::sort(enchantList.begin(), enchantList.end(), sort_enchant());
//
//		for (std::vector<SpellItemEnchantmentEntry const*>::const_iterator itr = enchantList.begin(); itr != enchantList.end(); ++itr)
//		{
//			std::ostringstream spellStr;
//			std::string name = (*itr)->description;
//			SpellEntry const* spell = sSpellStore.LookupEntry((*itr)->spellid[0]);
//			std::string icon = itemIconPrefix + "Spell_Holy_GreaterHeal";
//			if (spell)
//			{
//				SpellIconEntry const* spellIcon = sSpellIconStore.LookupEntry(spell->SpellIconID);
//				if (spellIcon->ID != 1)
//					icon = itemIconPrefix + "Spell_Holy_GreaterHeal";
//			}
//			spellStr << icon << iconSuffix << name;
//			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, spellStr.str(), 0, (*itr)->ID);
//		}*/
//	}
//	
//	struct sort_suffix 
//	{
//		bool operator()(ItemRandomPropertiesEntry const* lhs, ItemRandomPropertiesEntry const* rhs)
//		{
//			std::string name1 = lhs->nameSuffix;
//			std::string name2 = rhs->nameSuffix;
//			std::string search = "the ";
//			size_t index = name1.find(search);
//			
//			if (index != std::string::npos)
//				name1.erase(index, search.length());
//				
//			index = name2.find(search);
//			
//			if (index != std::string::npos)
//				name2.erase(index, search.length());
//				
//			return (name1 < name2) || ((name1 == name2) && (lhs->enchant_id[0] < rhs->enchant_id[0])) || ((name1 == name2) && (lhs->enchant_id[0] == lhs->enchant_id[0]) && (lhs->enchant_id[1] < rhs->enchant_id[1]));
//		}
//	};
//	
//	struct sort_suffix2 
//	{
//		bool operator()(ItemRandomSuffixEntry const* lhs, ItemRandomSuffixEntry const* rhs)
//		{
//			std::string name1 = lhs->nameSuffix;
//			std::string name2 = rhs->nameSuffix;
//			std::string search = "the ";
//			size_t index = name1.find(search);
//			
//			if (index != std::string::npos)
//				name1.erase(index, search.length());
//				
//			index = name2.find(search);
//			
//			if (index != std::string::npos)
//				name2.erase(index, search.length());
//				
//			return name1 < name2;
//		}
//	};
//
//	void GenerateSuffixList(Player* player, const ItemTemplate* itemp)
//	{
//		player->_twinkTabletData.textId = 50003;
//		if (itemp->RandomProperty > 0)
//		{
//			std::vector<ItemRandomPropertiesEntry const*> enchantModList = GetRandomPropertiesList(itemp->RandomProperty);
//			std::sort(enchantModList.begin(), enchantModList.end(), sort_suffix());
//			std::vector<ItemRandomPropertiesEntry const*>::const_iterator itr;
//			std::vector<ItemRandomPropertiesEntry const*>::const_iterator itr2;
//
//			for (itr = enchantModList.begin(), itr2 = enchantModList.begin() + 1; itr != enchantModList.end(); ++itr)
//			{
//				if (itr2 == enchantModList.end())
//				{
//					std::string suffixName = (*itr)->nameSuffix;
//					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "..." + suffixName, 0, (*itr)->ID);
//					break;
//				}
//				if (strcmp((*itr)->nameSuffix, (*itr2)->nameSuffix) != 0)
//				{
//					std::string suffixName = (*itr)->nameSuffix;
//					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "..." + suffixName, 0, (*itr)->ID);
//				}
//				itr2++;
//			}
//		}
//		else
//        {
//			std::vector<ItemRandomSuffixEntry const*> enchantModList = GetRandomSuffixList(itemp->RandomSuffix);
//			std::sort(enchantModList.begin(), enchantModList.end(), sort_suffix2());
//			
//			for (std::vector<ItemRandomSuffixEntry const*>::const_iterator itr = enchantModList.begin(); itr != enchantModList.end(); ++itr)
//			{
//				std::string suffixName = (*itr)->nameSuffix;
//				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "..." + suffixName, 0, (*itr)->ID);
//			}
//		}
//	}
//	
//	struct sort_glyph 
//	{
//		bool operator()(ItemTemplate const* lhs, ItemTemplate const* rhs)
//		{
//			SpellEffectEntry const* pSpellLhs = sSpellEffectStore.LookupEntry(lhs->Spells[0].SpellId);
//			SpellEffectEntry const* pSpellRhs = sSpellEffectStore.LookupEntry(rhs->Spells[0].SpellId);
//			GlyphPropertiesEntry const* gpLhs = sGlyphPropertiesStore.LookupEntry(pSpellLhs->EffectMiscValue);
//			GlyphPropertiesEntry const* gpRhs = sGlyphPropertiesStore.LookupEntry(pSpellRhs->EffectMiscValue);
//			return gpLhs->TypeFlags == gpRhs->TypeFlags ? lhs->Name1 < rhs->Name1 : gpLhs->TypeFlags < gpRhs->TypeFlags;
//		}
//	};
//	
//	bool GetGlyphsForClass(Player* player)
//	{
//		player->_twinkTabletData.textId = 50003;
//		player->_twinkTabletData.paMode = PA_MODE_GLYPHS;
//		VendorItemData const* items = sObjectMgr->GetNpcVendorItemList(glyphVendorEntry + player->getClass());
//		if (!items)
//			return false;
//		std::vector<ItemTemplate const*> itemVec;
//		for (uint8 i = 0; i < items->GetItemCount(); i++)
//		{
//			if (VendorItem const* item = items->GetItem(i))
//			{
//				if (ItemTemplate const* itemp = sObjectMgr->GetItemTemplate(item->item))
//				{
//					itemVec.push_back(itemp);
//				}
//			}
//		}
//
//		std::sort(itemVec.begin(), itemVec.end(), sort_glyph());
//
//		player->CLOSE_GOSSIP_MENU();
//		player->GetSession()->SetCurrentVendor(gearVendorEntry);
//		uint8 count = 0;
//		WorldPacket data(SMSG_LIST_INVENTORY, 8 + 1 + itemVec.size() * 8 * 4);
//		data << uint64(player->GetGUID());
//		size_t countPos = data.wpos();
//		data << uint8(count);
//
//		for (std::vector<ItemTemplate const*>::const_iterator itr = itemVec.begin(); itr != itemVec.end(); ++itr)
//		{
//			//GOSSIP BASED GLYPH MENU
//			if (SpellEffectEntry const* pSpell = sSpellEffectStore.LookupEntry((*itr)->Spells[0].SpellId))
//			{
//				if (GlyphPropertiesEntry const* gp = sGlyphPropertiesStore.LookupEntry(pSpell->EffectMiscValue))
//				{
//					uint32 spellId = gp->SpellId;
//					std::ostringstream str;
//					ItemDisplayInfoEntry const* itemDisplay = sItemDisplayInfoStore.LookupEntry((*itr)->DisplayInfoID);
//					bool glyphEquipped = player->HasAura(spellId);
//					//str << itemIconPrefix << itemDisplay->m_inventoryIcon << iconSuffix << "|c" << std::hex << statusColors[glyphEquipped ? 1 : 0] << (*itr)->Name1;
//					//player->ADD_GOSSIP_ITEM(0, str.str(), gp->TypeFlags, pSpell->Id);
//					//VENDOR BASED GLYPH MENU
//					data << uint32(count + 1);
//					data << uint32((*itr)->ItemId);
//					data << uint32((*itr)->DisplayInfoID);
//					data << int32(glyphEquipped ? -1 : 0); //stock
//					data << uint32(0); //price
//					data << uint32((*itr)->MaxDurability); //durability
//					data << uint32((*itr)->BuyCount); //buy count
//					data << uint32(0); // extended cost id
//					if (++count >= MAX_VENDOR_ITEMS)
//						break;
//				}
//			}
//
//		}
//
//		if (count == 0)
//		{
//			data << uint8(0);
//			player->GetSession()->SendPacket(&data);
//			return false;
//		}
//
//		data.put<uint8>(countPos, count);
//		player->GetSession()->SendPacket(&data);
//		return !itemVec.empty();
//	}
//
//	struct sort_gear 
//	{
//		bool operator()(ItemTemplate const* lhs, ItemTemplate const* rhs)
//		{
//			return lhs->Name1 < rhs->Name1;
//		}
//	};
//	
//	bool GetGearForInventoryType(Player* player, uint32 inventoryType)
//	{
//		player->_twinkTabletData.textId = 50003;
//		player->_twinkTabletData.paMode = PA_MODE_GEARMENU;
//		VendorItemData const* items = sObjectMgr->GetNpcVendorItemList(gearVendorEntry);
//		if (!items)
//			return false;
//
//		std::vector<uint32> multitype;
//		multitype.push_back(inventoryType);
//		if (inventoryType == 13)
//		{
//			multitype.push_back(17);
//			multitype.push_back(21);
//			multitype.push_back(22);
//		}
//		else if (inventoryType == 15)
//		{
//			multitype.push_back(25);
//			multitype.push_back(26);
//		}
//		else if (inventoryType == 5)
//			multitype.push_back(20);
//
//		std::vector<ItemTemplate const*> itemsVec;
//
//		uint8 itemCount = items->GetItemCount();
//		for (uint8 i = 0; i < itemCount; i++)
//		{
//			if (VendorItem const* item = items->GetItem(i))
//				if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(item->item))
//					for (uint8 j = 0; j < multitype.size(); j++)
//					{
//						uint16 dest;
//						Item* item = Item::CreateItem(itemTemplate->ItemId, 1, player);
//						if (player->getClass() == CLASS_HUNTER && itemTemplate->SubClass == 3 && itemTemplate->Quality != ITEM_QUALITY_HEIRLOOM) //hack
//							continue;
//						if (itemTemplate->InventoryType == multitype[j])
//						{
//							InventoryResult res = player->CanEquipItem(NULL_SLOT, dest, item, true, true);
//							if (itemTemplate->InventoryType == 23 || 14)
//							{
//								if (player->getClass() == 11)
//								{
//									if (res == EQUIP_ERR_2HANDED_EQUIPPED)
//										itemsVec.push_back(itemTemplate);
//								}
//								if (player->getClass() != 11)
//									if (res == EQUIP_ERR_2HANDED_EQUIPPED)
//										if (itemTemplate->AllowableClass != 1024)
//											itemsVec.push_back(itemTemplate);
//							}
//							if (res == EQUIP_ERR_OK)
//								itemsVec.push_back(itemTemplate);
//						}
//						delete item;
//					}
//		}
//
//		std::sort(itemsVec.begin(), itemsVec.end(), sort_gear());
//
//		player->CLOSE_GOSSIP_MENU();
//		player->GetSession()->SetCurrentVendor(gearVendorEntry);
//		uint8 count = 0;
//		WorldPacket data(SMSG_LIST_INVENTORY, 8 + 1 + itemsVec.size() * 8 * 4);
//		data << uint64(player->GetGUID());
//		size_t countPos = data.wpos();
//		data << uint8(count);
//
//
//		for (std::vector<ItemTemplate const*>::const_iterator itr = itemsVec.begin(); itr != itemsVec.end(); ++itr)
//		{
//			//GOSSIP BASED ITEM SELECTION
//			/*ItemDisplayInfoEntry const* displayInfo = sItemDisplayInfoStore.LookupEntry((*itr)->DisplayInfoID);
//			std::string icon = displayInfo->m_inventoryIcon;
//			uint32 itemId = (*itr)->ItemId;
//			uint32 color = ItemQualityColorsDark[(*itr)->Quality];
//			std::ostringstream itemStr;
//			std::string name = (*itr)->Name1;
//			itemStr << itemIconPrefix << icon << iconSuffix << "|c" << std::hex << color << name;
//			player->ADD_GOSSIP_ITEM(0, itemStr.str(), 0, itemId);*/
//
//			//VENDOR BASED ITEM SELECTION
//			data << uint32(count + 1);
//			data << uint32((*itr)->ItemId);
//			data << uint32((*itr)->DisplayInfoID);
//			data << int32(-1); //stock
//			data << uint32(0); //price
//			data << uint32((*itr)->MaxDurability); //durability
//			data << uint32((*itr)->BuyCount); //buy count
//			data << uint32(0); // extended cost id
//			if (++count >= MAX_VENDOR_ITEMS)
//				break;
//		}
//
//		if (count == 0)
//		{
//			data << uint8(0);
//			player->GetSession()->SendPacket(&data);
//			return false;
//		}
//
//		data.put<uint8>(countPos, count);
//		player->GetSession()->SendPacket(&data);
//		return !itemsVec.empty();
//	}
//
//	void PrepCharacterMenu(Player* player)
//	{
//		player->_twinkTabletData.textId = 50001;
//		player->PlayerTalkClass->ClearMenus();
//		player->_twinkTabletData.paMode = PA_MODE_CHARACTER;
//		player->ADD_GOSSIP_ITEM(0, "|TInterface\\Icons\\inv_inscription_tradeskill01:20:20:-21:0|tManage Profession Spells", 0, 0);
//		player->ADD_GOSSIP_ITEM(0, "|TInterface\\Icons\\inv_glyph_majorpaladin:20:20:-21:0|tManage Glyphs", 0, 1);
//		player->ADD_GOSSIP_ITEM(0, "|TInterface\\Icons\\ability_marksmanship:20:20:-21:0|tUnlearn Talents", 0, 2);
//		if (player->getClass() == CLASS_HUNTER)
//			player->ADD_GOSSIP_ITEM(0, "|TInterface\\Icons\\ability_hunter_pet_raptor:20:20:-21:0|tHunter Pets", 0, 3);
//
//	}
//
//	void PrepMainMenu(Player* player)
//	{
//		player->_twinkTabletData.textId = 50001;
//		player->PlayerTalkClass->ClearMenus();
//		player->_twinkTabletData.paMode = PA_MODE_MAINMENU;
//		player->ADD_GOSSIP_ITEM(0, "|TInterface\\Icons\\inv_helmet_31:20:20:-21:0|tGear", 0, 0);
//		player->ADD_GOSSIP_ITEM(0, "|Tinterface\\Icons\\achievement_arena_2v2_4:20:20:-21:0|tPvP", 0, 1);
//		player->ADD_GOSSIP_ITEM(0, "|Tinterface\\icons\\inv_misc_gear_01:20:20:-21:0|tCharacter Setup", 0, 3);
//		player->ADD_GOSSIP_ITEM(0, "|Tinterface\\icons\\inv_sword_27:20:20:-21:0|tGear Templates", 0, 4);
//		//player->ADD_GOSSIP_ITEM(0, "Options", 0, 2);
//	}
//
//	void PrepGearMenu(Player* player)
//	{
//		player->_twinkTabletData.paMode = PA_MODE_INVENTORYTYPE;
//		player->_twinkTabletData.textId = 50002;
//		//GOSSIP BASED inventory type list
//		//for (uint32 i = 1; i < inventoryTypes.size(); i++)
//		//{ //{ "none", "Head", "Neck", "Shoulder", "Shirt", "Chest", "Waist", "Legs", "Feet", "Wrists", "Hands", "Finger", "Trinket", "Weapon", "Shield", "Ranged", "Back", "Two-Hand", "Bag",
//		//  //     0      1        2       3           4       5       6           7       8       9          10       11      12          13          14      15         16       17        18
//		//  //"Tabard", "Robe", "Mainhand", "Offhand", "Holdable", "Ammo", "Thrown", "Wands,Guns", "Quiver", "Relic"
//		//  //  19         20       21          22          23        24       25       26          27          28
//		//	if (i == 4 || i >= 17  && i <= 22 || i >= 24)
//		//		continue;
//		//	player->ADD_GOSSIP_ITEM(0, inventoryTypes[i], 0, i);
//		//}
//		//VENDOR BASED inventory types list
//		VendorItemData const* items = sObjectMgr->GetNpcVendorItemList(inventoryTypeVendorEntry);
//		if (!items)
//			return;
//
//		uint8 itemCount = items->GetItemCount();
//		player->CLOSE_GOSSIP_MENU();
//		player->GetSession()->SetCurrentVendor(inventoryTypeVendorEntry);
//		uint8 count = 0;
//		WorldPacket data(SMSG_LIST_INVENTORY, 8 + 1 + itemCount * 8 * 4);
//		data << uint64(player->GetGUID());
//		size_t countPos = data.wpos();
//		data << uint8(count);
//
//		for (uint8 i = 0; i < itemCount; i++)
//		{
//			if (VendorItem const* item = items->GetItem(i))
//				if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(item->item))
//				{
//					data << uint32(count + 1);
//					data << uint32(itemTemplate->ItemId);
//					data << uint32(itemTemplate->DisplayInfoID);
//					data << int32(-1); //stock
//					data << uint32(0); //price
//					data << uint32(itemTemplate->MaxDurability); //durability
//					data << uint32(itemTemplate->BuyCount); //buy count
//					data << uint32(0); // extended cost id
//					if (++count >= MAX_VENDOR_ITEMS)
//						break;
//				}
//		}
//		if (count == 0)
//		{
//			data << uint8(0);
//			player->GetSession()->SendPacket(&data);
//			return;
//		}
//
//		data.put<uint8>(countPos, count);
//		player->GetSession()->SendPacket(&data);
//	}
//
//	void PrepPvpMenu(Player* player)
//	{
//		player->_twinkTabletData.paMode = PA_MODE_PVPMENU;
//		player->_twinkTabletData.textId = 50003;
//		player->ADD_GOSSIP_ITEM(0, "|TInterface\\Icons\\achievement_arena_2v2_5:20:20:-21:0|tJoin Arena Skrimish (Solo)", 0, 0);
//		if (Group* group = player->GetGroup())
//			player->ADD_GOSSIP_ITEM(0, "|TInterface\\Icons\\achievement_arena_2v2_7:20:20:-21:0|tJoin Arena Skirmish (Group)", 0, 1);
//	}
//
//	void PrepArenaMenu(Player* player)
//	{
//		player->_twinkTabletData.textId = 50003;
//		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "|TInterface\\Icons\\achievement_arena_2v2_1:20:20:-21:0|t2v2", 0, 0);
//		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "|TInterface\\Icons\\achievement_arena_3v3_1:20:20:-21:0|t3v3", 0, 1);
//		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "|TInterface\\Icons\\achievement_arena_5v5_1:20:20:-21:0|t5v5", 0, 2);
//	}
//
//	void PrepHunterPetMenu(Player* player)
//	{
//		player->_twinkTabletData.paMode = PA_MODE_HUNTERPET;
//		player->_twinkTabletData.textId = 50003;
//		for (uint32 i = 0; i < sizeof(HunterPetIds) / sizeof(HunterPetIds[0]); i++)
//		{
//			CreatureTemplate const* cTemp = sObjectMgr->GetCreatureTemplate(HunterPetIds[i]);
//			if (!cTemp)
//				continue;
//			CreatureFamilyEntry const* family = sCreatureFamilyStore.LookupEntry(cTemp->family);
//			if (!family)
//				continue;
//			std::ostringstream str;
//			str << iconPrefix << family->icon << iconSuffix << family->Name[0];
//			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, str.str(), 0, HunterPetIds[i]);
//		}
//	}
//
//	/*void PrepSetupMenu(Player* player)
//	{
//	player->_twinkTabletData.paMode = PA_MODE_SETUP;
//	player->_twinkTabletData.textId = 50000;
//	player->ADD_GOSSIP_ITEM(0, "Yes", 0, 0);
//	player->ADD_GOSSIP_ITEM(0, "No", 0, 1);
//	}*/
//
//	void PrepGearTemplates(Player* player)
//	{
//		player->_twinkTabletData.paMode = PA_MODE_GEARTEMPLATES;
//		GearTemplatesContainer const* gTemps = sObjectMgr->GetGearTemplates();
//		for (GearTemplatesContainer::const_iterator itr = gTemps->begin(); itr != gTemps->end(); ++itr)
//		{
//			if ((*itr).second.classId != player->getClass())
//				continue;
//			std::ostringstream str;
//			std::string icon = iconPrefix + (*itr).second.icon + iconSuffix;
//			str << icon << (*itr).second.name;
//			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, str.str(), 0, (*itr).first);
//		}
//		if (player->TabletCompletedSetup())
//		{
//			player->_twinkTabletData.textId = 50000;
//		}
//		else
//		{
//			player->_twinkTabletData.textId = 50001;  //Welcome Message
//			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "I don't want a template", 0, 0);
//		}
//	}
//
//	void PrepProfessions(Player* player)
//	{
//		/*player->_twinkTabletData.paMode = PA_MODE_PROFS;
//		player->_twinkTabletData.textId = 50003;
//		uint8 profCount = 0;
//		for (uint8 i = 0; i < sizeof(profSpells) / sizeof(profSpells[0]); i++)
//		{
//			if (player->HasSpell(profSpells[i]))
//				profCount++;
//		}
//		for (uint8 i = 0; i < sizeof(profSpells) / sizeof(profSpells[0]); i++)
//		{
//			if (player->HasSpell(profSpells[i]))
//				continue;
//			std::ostringstream str;
//			SpellEntry const* spellEntry = sSpellStore.LookupEntry(profSpells[i]);
//			std::string name = spellEntry->SpellName;
//			SpellIconEntry const* iconEntry = sSpellIconStore.LookupEntry(spellEntry->SpellIconID);
//			std::string icon = iconPrefix + iconEntry->m_spellIcon + iconSuffix;
//			str << icon << "|c" << std::hex << statusColors[profCount < 2 ? 1 : 0] << name;
//			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, str.str(), profCount, profSpells[i]);
//		}
//		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface\\Icons\\spell_nature_wispsplode:20:20:-21:0|tUnlearn All", 100, 0);*/
//	}
//
//	/*void PrepOptions(Player* player)
//	{
//	player->_twinkTabletData.paMode = PA_MODE_OPTIONS;
//	player->_twinkTabletData.textId = 50003;
//	std::ostringstream str;
//	str << paOptions[0] << " - |c" << std::hex;
//	if (player->TabletFilterList())
//	str << statusColors[1] << "Enabled";
//	else
//	str << statusColors[0] << "Disabled";
//	player->ADD_GOSSIP_ITEM(0, str.str(), 0, 0);
//	str.str("");
//	str << paOptions[1] << " - |c" << std::hex;
//	if (player->TabletDetailedItems())
//	str << statusColors[1] << "Enabled";
//	else
//	str << statusColors[0] << "Disabled";
//	player->ADD_GOSSIP_ITEM(0, str.str(), 0, 1);
//	}*/
//
//	void CreateHunterPet(Player* player, uint32 id)
//	{
//		if (player->IsInCombat(player))
//		{
//			ChatHandler(player->GetSession()).SendSysMessage("You can't summon a pet while in combat!");
//			return;
//		}
//
//		if (Battleground* bg = player->GetBattleground())
//			if (bg->GetStatus() == STATUS_IN_PROGRESS)
//			{
//				ChatHandler(player->GetSession()).SendSysMessage("You can't summon a pet while a battle is in progress!");
//				return;
//			}
//
//		Creature* currentPet = player->GetPet();
//		if (currentPet)
//		{
//			player->RemovePet((Pet*)currentPet, PET_SLOT_DELETED);
//		}
//		Creature* tmp = player->SummonCreature(id, player->GetPositionX(), player->GetPositionY(), player->GetPositionZ());
//		Pet* pet = new Pet(player, HUNTER_PET);
//
//		if (!pet)
//			return;
//
//		if (!pet->CreateBaseAtCreature(tmp))
//		{
//			delete pet;
//			return;
//		}
//
//		tmp->setDeathState(JUST_DIED);
//		tmp->RemoveCorpse();
//		tmp->SetHealth(0);
//
//		pet->SetUInt64Value(UNIT_FIELD_CREATEDBY, player->GetGUID());
//		pet->SetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE, player->getFaction());
//
//		if (!pet->InitStatsForLevel(player->getLevel()))
//		{
//			delete pet;
//			return;
//		}
//		pet->SetUInt32Value(UNIT_FIELD_LEVEL, 18);
//		pet->GetCharmInfo()->SetPetNumber(sObjectMgr->GeneratePetNumber(), true);
//		pet->InitPetCreateSpells();
//		pet->SetHealth(pet->GetMaxHealth());
//		pet->GetMap()->AddToMap(pet->ToCreature());
//		pet->SetUInt32Value(UNIT_FIELD_LEVEL, player->getLevel());
//		player->SetMinion(pet, true, PET_SLOT_DEFAULT);
//		pet->SavePet(PET_SLOT_DEFAULT);
//		player->PetSpellInitialize();
//	}
//
//	bool ValidateSelection(Player* player, Item* item, uint32 sender, uint32 action)
//	{
//		return true;
//	}
//
//	bool OnUse(Player* player, Item* item, SpellCastTargets const& targets)
//	{
//		player->_twinkTabletData.itemId = 0;
//		player->_twinkTabletData.suffixId = 0;
//		player->_twinkTabletData.enchantId = 0;
//		player->_twinkTabletData.textId = DEFAULT_GOSSIP_MESSAGE;
//		if (!player->TabletCompletedSetup())
//		{
//			player->PlayerTalkClass->ClearMenus();
//			PrepGearTemplates(player);
//		}
//		else
//		{
//			player->PlayerTalkClass->ClearMenus();
//			PrepMainMenu(player);
//		}
//		player->SEND_GOSSIP_MENU(player->_twinkTabletData.textId, item->GetGUID());
//		return true;
//	}
//
//	void EndGearGossip(Player* player)
//	{
//		ItemTemplate const* itemp = sObjectMgr->GetItemTemplate(player->_twinkTabletData.itemId);
//		int8 neg = itemp->RandomSuffix ? -1 : 1;
//		ItemPosCountVec dest;
//		InventoryResult msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, player->_twinkTabletData.itemId, 1);
//		if (msg == EQUIP_ERR_OK)
//		{
//			Item* item = player->StoreNewItem(dest, player->_twinkTabletData.itemId, true, player->_twinkTabletData.suffixId*neg);
//			if (player->_twinkTabletData.enchantId)
//			{
//				player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, false);
//				item->SetEnchantment(PERM_ENCHANTMENT_SLOT, player->_twinkTabletData.enchantId, 0, 0);
//				player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, true);
//			}
//			player->SendNewItem(item, 1, true, false);
//		}
//		else
//			player->SendEquipError(msg, NULL, NULL, player->_twinkTabletData.itemId);
//		player->_twinkTabletData.itemId = 0;
//		player->_twinkTabletData.suffixId = 0;
//		player->_twinkTabletData.enchantId = 0;
//		PrepGearMenu(player);
//		player->_twinkTabletData.paMode = PA_MODE_INVENTORYTYPE;
//	}
//
//	void GiveGearTemplate(Player* player, uint32 gearTemplateId)
//	{
//		ApplyGearTemplateExtra(player, gearTemplateId);
//		if (GearTemplateEntry const* gTemplate = sObjectMgr->GetGearTemplateById(gearTemplateId))
//		{
//			for (uint32 i = 0; i < gTemplate->gearInfo.size(); i++)
//			{
//				uint32 itemId = gTemplate->gearInfo[i].itemId;
//				uint32 enchantId = gTemplate->gearInfo[i].enchantId;
//				int32 suffixId = gTemplate->gearInfo[i].suffixId;
//				if (player->GetTeam() == HORDE)
//				{
//					if (itemId == 44098)
//						itemId = 44097;// insignia trinket
//					else if (itemId == 20444)
//						itemId = 20442;// scouts medallion
//					else if (itemId == 20439)
//						itemId = 20429;//legionnaires band
//					else if (itemId == 20431)
//						itemId = 20426; // advisor's ring
//				}
//
//				ItemTemplate const* itemp = sObjectMgr->GetItemTemplate(itemId);
//				ItemPosCountVec idest;
//				InventoryResult msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, idest, itemId, 1);
//				if (msg == EQUIP_ERR_OK)
//				{
//					int8 neg = 1;
//					if (itemp->RandomSuffix)
//						neg = -1;
//					Item* item = player->StoreNewItem(idest, itemId, true, suffixId*neg);
//					if (enchantId)
//					{
//						player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, false);
//						WorldPacket data(SMSG_ENCHANTMENTLOG, (8 + 8 + 4 + 4));     // last check 2.0.10
//						data.appendPackGUID(player->GetGUID());
//						data.appendPackGUID(0);
//						data << uint32(itemId);
//						data << uint32(enchantId);
//						player->SendMessageToSet(&data, true);
//						item->SetUInt32Value(ITEM_FIELD_ENCHANTMENT_1_1 + PERM_ENCHANTMENT_SLOT*MAX_ENCHANTMENT_OFFSET + ENCHANTMENT_ID_OFFSET, enchantId);
//						item->SetUInt32Value(ITEM_FIELD_ENCHANTMENT_1_1 + PERM_ENCHANTMENT_SLOT*MAX_ENCHANTMENT_OFFSET + ENCHANTMENT_DURATION_OFFSET, 0);
//						item->SetUInt32Value(ITEM_FIELD_ENCHANTMENT_1_1 + PERM_ENCHANTMENT_SLOT*MAX_ENCHANTMENT_OFFSET + ENCHANTMENT_CHARGES_OFFSET, 0);
//						item->SetState(ITEM_CHANGED, player);
//						player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, true);
//					}
//					uint8 srcbag = item->GetBagSlot();
//					uint8 srcslot = item->GetSlot();
//					if (!item->IsEquipped())
//					{
//						uint16 dest;
//						InventoryResult msg = player->CanEquipItem(NULL_SLOT, dest, item, !item->IsBag());
//						if (msg != EQUIP_ERR_OK)
//							continue;
//						uint16 src = item->GetPos();
//						if (dest == src)                                           // prevent equip in same slot, only at cheat
//							return;
//						Item* pDstItem = player->GetItemByPos(dest);
//						if (!pDstItem)                                         // empty slot, simple case
//						{
//							player->RemoveItem(srcbag, srcslot, true);
//							player->EquipItem(dest, item, true);
//							player->AutoUnequipOffhandIfNeed();
//						}
//						else                                                    // have currently equipped item, not simple case
//						{
//							uint8 dstbag = pDstItem->GetBagSlot();
//							uint8 dstslot = pDstItem->GetSlot();
//
//							msg = player->CanUnequipItem(dest, !item->IsBag());
//							if (msg != EQUIP_ERR_OK)
//							{
//								player->SendEquipError(msg, pDstItem, NULL);
//								return;
//							}
//
//							// check dest->src move possibility
//							ItemPosCountVec sSrc;
//							uint16 eSrc = 0;
//							if (player->IsInventoryPos(src))
//							{
//								msg = player->CanStoreItem(srcbag, srcslot, sSrc, pDstItem, true);
//								if (msg != EQUIP_ERR_OK)
//									msg = player->CanStoreItem(srcbag, NULL_SLOT, sSrc, pDstItem, true);
//								if (msg != EQUIP_ERR_OK)
//									msg = player->CanStoreItem(NULL_BAG, NULL_SLOT, sSrc, pDstItem, true);
//							}
//							else if (player->IsBankPos(src))
//							{
//								msg = player->CanBankItem(srcbag, srcslot, sSrc, pDstItem, true);
//								if (msg != EQUIP_ERR_OK)
//									msg = player->CanBankItem(srcbag, NULL_SLOT, sSrc, pDstItem, true);
//								if (msg != EQUIP_ERR_OK)
//									msg = player->CanBankItem(NULL_BAG, NULL_SLOT, sSrc, pDstItem, true);
//							}
//							else if (player->IsEquipmentPos(src))
//							{
//								msg = player->CanEquipItem(srcslot, eSrc, pDstItem, true);
//								if (msg == EQUIP_ERR_OK)
//									msg = player->CanUnequipItem(eSrc, true);
//							}
//
//							if (msg != EQUIP_ERR_OK)
//							{
//								player->SendEquipError(msg, pDstItem, item);
//								return;
//							}
//
//							// now do moves, remove...
//							player->RemoveItem(dstbag, dstslot, false);
//							player->RemoveItem(srcbag, srcslot, false);
//
//							// add to dest
//							player->EquipItem(dest, item, true);
//
//							// add to src
//							if (player->IsInventoryPos(src))
//								player->StoreItem(sSrc, pDstItem, true);
//							else if (player->IsBankPos(src))
//								player->BankItem(sSrc, pDstItem, true);
//							else if (player->IsEquipmentPos(src))
//								player->EquipItem(eSrc, pDstItem, true);
//
//							player->AutoUnequipOffhandIfNeed();
//						}
//					}
//				}
//			}
//		}
//	}
//
//	void ApplyGearTemplateExtra(Player* player, uint32 gearTemplateId)
//	{
//		for (uint8 i = 0; i < sizeof(profSpells) / sizeof(profSpells[0]); i++) // unlearn profession spells
//		{
//			player->removeSpell(profSpells[i], false, false);
//		}
//		player->ResetTalents(true);  // reset talents
//		player->SendTalentsInfoData(false);
//		if (std::vector<GearTemplateExtraEntry> const* gearTemplateExtraVec = sObjectMgr->GetGearTemplateExtra(gearTemplateId))
//		{
//			for (std::vector<GearTemplateExtraEntry>::const_iterator itr = gearTemplateExtraVec->begin(); itr != gearTemplateExtraVec->end(); ++itr)
//			{
//				switch (itr->type)
//				{
//				case 0: //Talents
//				{
//					uint8 rank = itr->data_1 > 0 ? itr->data_1 - 1 : 0;
//					player->LearnTalent(itr->data_0, itr->data_1 - 1);
//					break;
//				}
//				case 1: //Glyphs
//				{
//					if (uint32 oldglyph = player->GetGlyph(1, itr->data_1))
//					{
//						if (GlyphPropertiesEntry const* old_gp = sGlyphPropertiesStore.LookupEntry(oldglyph))
//						{
//							player->RemoveAurasDueToSpell(old_gp->SpellId);
//							player->SetGlyph(itr->data_1, 0);
//						}
//					}
//					SpellEffectEntry const* pSpell = sSpellEffectStore.LookupEntry(itr->data_0);
//					GlyphPropertiesEntry const* gp = sGlyphPropertiesStore.LookupEntry(pSpell->EffectMiscValue);
//					player->CastSpell(player, gp->SpellId, true);
//					player->SetGlyph(itr->data_1, pSpell->EffectMiscValue);
//					break;
//				}
//
//				case 2: //Hunter pet
//				{
//					if (player->getClass() == CLASS_HUNTER)
//						CreateHunterPet(player, itr->data_0);
//					break;
//				}
//
//				case 3: //Profession Spells
//				{
//					for (uint8 i = 0; i < sizeof(profSpells) / sizeof(profSpells[0]); i++)
//					{
//						if (profSpells[i] == itr->data_0)
//							player->learnSpell(itr->data_0, false);
//					}
//				}
//				}
//			}
//			player->SendTalentsInfoData(false);
//		}
//	}
//
//	void OnGossipSelect(Player* player, Item* item, uint32 sender, uint32 action)
//	{
//		bool send = true;
//		if (!ValidateSelection(player, item, sender, action))
//			return;
//
//		player->PlayerTalkClass->ClearMenus();
//		if (sender == 999 && action == 999)
//		{
//			switch (player->_twinkTabletData.paMode)
//			{
//			case PA_MODE_CHARACTER:
//			case PA_MODE_INVENTORYTYPE:
//			case PA_MODE_PVPMENU:
//			case PA_MODE_OPTIONS:
//			case PA_MODE_GEARTEMPLATES:
//				PrepMainMenu(player);
//				break;
//			//case PA_MODE_ARENA:
//				PrepPvpMenu(player);
//				break;
//			case PA_MODE_PROFS:
//			//case PA_MODE_GLYPHS:
//			case PA_MODE_HUNTERPET:
//				PrepCharacterMenu(player);
//				break;
//			case PA_MODE_GEARMENU:
//			case PA_MODE_SUFFIX:
//			case PA_MODE_ENCHANT:
//				player->_twinkTabletData.itemId = 0;
//				player->_twinkTabletData.suffixId = 0;
//				player->_twinkTabletData.enchantId = 0;
//				PrepGearMenu(player);
//				break;
//			}
//		}
//		else
//		{
//			switch (player->_twinkTabletData.paMode)
//			{
//			case PA_MODE_MAINMENU:
//			{
//				if (action == 0) // GEAR
//				{
//					PrepGearMenu(player);
//					send = false;
//				}
//				else if (action == 1) // PVP
//					PrepPvpMenu(player);
//				//else if (action == 2) // OPTIONS
//				//PrepOptions(player);
//				else if (action == 3) // CHARACTER
//					PrepCharacterMenu(player);
//				else if (action == 4) //TEMPLATES
//					PrepGearTemplates(player);
//				break;
//			}
//
//			case PA_MODE_CHARACTER:
//			{
//				if (action == 0) // PROFESSIONS
//					PrepProfessions(player);
//				/*else if (action == 1) // GLYPHS
//				{
//					GetGlyphsForClass(player);
//					send = false;
//				}*/
//				else if (action == 2) // TALENTS
//				{
//					player->ResetTalents(true);
//					player->SendTalentsInfoData(false);
//					player->GetSession()->SendNotification(LANG_RESET_TALENTS);
//					PrepCharacterMenu(player);
//				}
//				else if (action == 3 && player->getClass() == CLASS_HUNTER)
//					PrepHunterPetMenu(player);
//				break;
//			}
//
//			case PA_MODE_INVENTORYTYPE:
//			{
//				if (!GetGearForInventoryType(player, action))
//					PrepGearMenu(player);
//				send = false;
//				break;
//			}
//
//			case PA_MODE_GEARMENU:
//			{
//				player->_twinkTabletData.itemId = action;
//				ItemTemplate const* itemp = sObjectMgr->GetItemTemplate(action);
//				if (itemp->RandomProperty || itemp->RandomSuffix)
//				{
//					GenerateSuffixList(player, itemp);
//					player->_twinkTabletData.paMode = PA_MODE_SUFFIX;
//				}
//				else if (InitEnchantList(action, player))
//				{
//					GenerateEnchantList(player);
//					player->_twinkTabletData.paMode = PA_MODE_ENCHANT;
//				}
//				else
//				{
//					EndGearGossip(player);
//					send = false;
//				}
//				break;
//			}
//
//			case PA_MODE_SUFFIX:
//			{
//				player->_twinkTabletData.suffixId = action;
//				if (InitEnchantList(player->_twinkTabletData.itemId, player))
//				{
//					GenerateEnchantList(player);
//					player->_twinkTabletData.paMode = PA_MODE_ENCHANT;
//				}
//				else
//				{
//					EndGearGossip(player);
//					send = false;
//				}
//				break;
//			}
//
//			case PA_MODE_ENCHANT:
//			{
//				player->_twinkTabletData.enchantId = action;
//				EndGearGossip(player);
//				send = false;
//				break;
//			}
//
//			case PA_MODE_GEARTEMPLATES:
//			{
//				if (!player->TabletCompletedSetup())
//					player->SetTabletCompletedSetup(true);
//				player->_twinkTabletData.paMode = PA_MODE_MAINMENU;
//				if (action > 0)
//					GiveGearTemplate(player, action);
//				PrepMainMenu(player);
//				break;
//			}
//
//			/*case PA_MODE_PVPMENU:
//			{
//				player->_twinkTabletData.paMode = PA_MODE_ARENA;
//				player->_twinkTabletData.pvpGroup = action > 0;
//				PrepArenaMenu(player);
//				break;
//			}*/
//
//			/*case PA_MODE_ARENA:
//			{
//				sBattlegroundMgr->ScheduleQueueUpdate(player, player->_twinkTabletData.pvpGroup, action, 0);
//				send = false;
//				break;
//			}*/
//
//			/*case PA_MODE_OPTIONS:
//			{
//			if (action == 0)
//			player->SetTabletFilterList(!player->TabletFilterList());
//			else if (action == 1)
//			player->SetTabletDetailedItems(!player->TabletDetailedItems());
//			PrepOptions(player);
//			break;
//			}*/
//
//			case PA_MODE_HUNTERPET:
//			{
//				if (CreatureTemplate const* cTemp = sObjectMgr->GetCreatureTemplate(action))
//					if (player->getClass() == CLASS_HUNTER && cTemp->isTameable(false))
//						CreateHunterPet(player, action);
//				send = false;
//				break;
//			}
//
//			case PA_MODE_PROFS:
//			{
//				if (sender < 2)
//					player->learnSpell(action, false);
//				else if (sender == 100)
//				{
//					for (uint8 i = 0; i < sizeof(profSpells) / sizeof(profSpells[0]); i++)
//					{
//						player->removeSpell(profSpells[i], false, false);
//					}
//				}
//				PrepProfessions(player);
//				break;
//			}
//
//			/*case PA_MODE_GLYPHS:
//			{
//				if (player->IsInCombat(player))
//				{
//					ChatHandler(player->GetSession()).SendSysMessage("You can't change glyphs while in combat!");
//					return;
//				}
//
//				if (Battleground* bg = player->GetBattleground())
//					if (bg->GetStatus() == STATUS_IN_PROGRESS)
//					{
//						ChatHandler(player->GetSession()).SendSysMessage("You can't change glyphs while a battle is in progress!");
//						return;
//					}
//				if (uint32 oldglyph = player->GetGlyph(sender, 0))
//				{
//					if (GlyphPropertiesEntry const* old_gp = sGlyphPropertiesStore.LookupEntry(oldglyph))
//					{
//						player->RemoveAurasDueToSpell(old_gp->SpellId);
//						player->SetGlyph(sender, 0);
//					}
//				}
//				if (SpellEffectEntry const* pSpell = sSpellEffectStore.LookupEntry(action))
//				{
//					if (GlyphPropertiesEntry const* gp = sGlyphPropertiesStore.LookupEntry(pSpell->EffectMiscValue))
//					{
//						player->CastSpell(player, gp->SpellId, true);
//						player->SetGlyph(sender, pSpell->EffectMiscValue);
//						player->SendTalentsInfoData(false);
//						GetGlyphsForClass(player);
//						send = false;
//					}
//				}
//			}*/
//			}
//		}
//		if (send)
//		{
//			if (player->TabletCompletedSetup() && player->_twinkTabletData.paMode != PA_MODE_MAINMENU)
//				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "|TInterface\\Icons\\trade_engineering:20:20:-21:0|tBack to Previous Menu", 999, 999);
//			player->SEND_GOSSIP_MENU(player->_twinkTabletData.textId, item->GetGUID());
//		}
//		else
//			player->CLOSE_GOSSIP_MENU();
//	}
//private:
//	//std::string paModeIcons[3];
//	//std::string inventoryTypes[29];
//	//std::array<std::string, 2> paOptions;
//};
//
//void AddSC_item_pa()
//{
//	new item_pa();
//}